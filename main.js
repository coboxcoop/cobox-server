const fs = require('fs')
const constants = require('cobox-constants')
const yargs = require('yargs')
const findUp = require('find-up')
const YAML = require('js-yaml')
const config = require('@coboxcoop/config')
const { printAppInfo }= require('./util')

const App = require('./index')
const options = require('./bin/lib/options')

const args = yargs
  .config(config.load())
  .options(options)
  .argv

if (require.main === module) return run(args)
else module.exports = run

function run (opts) {
  const app = App(opts)

  app.start((err) => {
    if (err) return console.error(err)

    var hostname = app.get('hostname')
    var port = app.get('port')
    var mount = app.get('mount')
    var storage = app.get('storage')
    var udpPort = app.get('udpPort')

    if (opts.withUi) openUI()

    var info = { hostname, port, udpPort, mount, storage }
    return printAppInfo(info)

    function openUI () {
      console.log()
      console.log(`launching the UI...`)
      require('open')(`http://${hostname}:${port}`)
    }
  })
}
