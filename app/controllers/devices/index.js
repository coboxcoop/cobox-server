const debug = require('debug')('cobox-server:devices')
const through = require('through2')
const pick = require('lodash.pick')
const collect = require('collect-stream')

const DEVICE_CONNECTED = 'DEVICE_CONNECTED'
const DEVICE_DISCONNECTED = 'DEVICE_DISCONNECTED'

const DevicesController = module.exports = (api) => {
  const Device = api.devices.store

  const controller = {
    live,
    index,
    show
  }

  return controller

  // -------------------- Controller Actions -------------------- //

  // WS /api/devices/.websocket
  function live (params, opts = {}) {
    return Device.createLiveStream()
      .pipe(format())
  }

  // GET /api/devices
  async function index (params, opts = {}) {
    return await new Promise((resolve, reject) => {
      const devices = Device.createReadStream()
        .pipe(format())

      collect(devices, (err, msgs) => {
        if (err) return reject(err)
        return resolve(msgs)
      })
    })
  }

  // GET /api/devices/:id
  async function show (params, opts = {}) {
    const device = await findDevice(params)
    if (!opts.decorate) return device
    return device.content
  }

  // --------------------- Helpers ---------------------- //

  function findDevice (params) {
    return new Promise((resolve, reject) => {
      Device.get(params.publicKey, (err, msg) => {
        if (err) return reject(err)
        if (err && err.notFound) return reject(new Error('device not seen on this network'))
        else resolve(msg)
      })
    })
  }
}

function format () {
  return through.obj(function (msg, _, next) {
    if (msg.sync) return next()

    var isBroadcasting = msg.value.content.broadcasting
    var address = msg.value.content.address

    this.push({
      type: isBroadcasting ? DEVICE_CONNECTED : DEVICE_DISCONNECTED,
      data: { address, ...pick(msg.value, ['author', 'timestamp']) }
    })

    next()
  })
}
