const debug = require('debug')('cobox-server')
const Invite = require('cobox-key-exchange')
const { loadKey } = require('cobox-keys')
const { encodings } = require('cobox-schemas')
const pick = require('lodash.pick')
const collect = require('collect-stream')

const SpaceDecorator = require('../../../../../lib/decorators/space')
const resourceParams = require('../../../../helpers/resource-params')
const ConnectionsController = require('../connections')
const { hex } = require('../../../../../util')

const PeerInvite = encodings.peer.invite
const PeerAbout = encodings.peer.about
const { assign } = Object

const InvitesController = module.exports = (api, devices) => {
  const controller = {
    accept,
    index,
    create
  }

  return controller

  // --------------- Controller Actions ---------------- //

  // GET /api/admin/devices/invites/accept
  async function accept (params, opts = {}) {
    let deviceParams

    try {
      deviceParams = Invite.open(Buffer.from(params.code, 'hex'), api.profile.identity)
    } catch (error) {
      throw new Error('invite code invalid')
    }

    let device = await devices.create(deviceParams)
    var decorator = SpaceDecorator(device)
    var _opts = assign({}, opts, resourceParams(decorator.toJSON()))
    return await devices.connections.create(null, _opts)
  }

  // GET /api/admin/devices/:id/invites
  async function index (params, opts = {}) {
    let device = await devices.show(resourceParams(params))

    return await new Promise((resolve, reject) => {
      const query = { $filter: { value: { type: 'peer/invite', timestamp: { $gt: 0 } } } }
      const invites = device.log.read({ query: [query] })

      collect(invites, (err, msgs) => {
        if (err) return reject(err)
        return resolve(msgs)
      })
    })
  }

  // POST /api/admin/devices/:id/invites
  async function create (params, opts = {}) {
    let device = await devices.show(resourceParams(opts))
    let code = Invite.create(inviteParams(device, params))

    let msg = {
      type: 'peer/invite',
      timestamp: Date.now(),
      author: hex(api.profile.publicKey),
      content: {
        code: hex(code),
        publicKey: hex(params.publicKey)
      }
    }

    await device.log.publish(PeerInvite.encode(msg))

    return msg
  }
}

function inviteParams (device, params) {
  var encryptionKey = loadKey(device.path, 'encryption_key')
  var deviceParams = assign(SpaceDecorator(device).toJSON(), params)
  var picked = pick(deviceParams, ['publicKey', 'address', 'name'])
  return assign({ encryptionKey }, picked)
}
