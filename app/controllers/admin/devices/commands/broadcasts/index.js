const debug = require('debug')('cobox-server:broadcasts')
const assert = require('assert')
const pick = require('lodash.pick')
const { encodings, validators } = require('cobox-schemas')
const collect =  require('collect-stream')

const Announce = encodings.command.announce
const Hide = encodings.command.hide
const isAnnounce = validators.command.announce
const isHide = validators.command.hide

const resourceParams = require('../../../../../helpers/resource-params')
const { hex } = require('../../../../../../util')

const ANNOUNCE = 'command/announce'
const HIDE = 'command/hide'

const BroadcastCommandsController = module.exports = (api, devices) => {
  return {
    index,
    create,
    update,
    destroy
  }

  // -------------------- Controller Actions -------------------- //

  // GET /api/admin/devices/:id/commands/broadcasts
  async function index (params, opts = {}) {
    let device = await devices.show(resourceParams(params))

    return await new Promise((resolve, reject) => {
      const query = { $filter: { value: { type: { $in: [ANNOUNCE, HIDE] }, timestamp: { $gt: 0 } } } }
      const commands = device.log.read({ query: [query] })

      collect(commands, (err, msgs) => {
        if (err) return reject(err)
        return resolve(msgs)
      })
    })
  }

  // POST /api/admin/devices/:id/commands/announce
  async function create (params, opts) {
    let device = await devices.show(resourceParams(params))

    const msg = {
      type: ANNOUNCE,
      timestamp: Date.now(),
      author: hex(api.profile.publicKey)
    }

    assert(isAnnounce(msg), 'invalid parameters')

    await device.log.publish(Announce.encode(msg))

    return msg
  }

  // POST /api/admin/devices/:id/commands/hide
  async function destroy (params, opts) {
    let device = await devices.show(resourceParams(params))

    const msg = {
      type: HIDE,
      timestamp: Date.now(),
      author: hex(api.profile.publicKey)
    }

    assert(isHide(msg), 'invalid parameters')

    await device.log.publish(Hide.encode(msg))

    return msg
  }

  // PATCH /api/admin/devices/:id/commands/broadcasts
  async function update (params, opts) {
    let commands = await index(params, opts)
    let command = commands[commands.length - 1]
    if (command === ANNOUNCE) return await create(params, opts)
    else if (command === HIDE) return await destroy(params, opts)
    else throw new Error('invalid command')
  }
}

