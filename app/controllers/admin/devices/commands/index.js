const CommandsController = module.exports = (api, devices) => ({
  broadcasts: require('./broadcasts')(api, devices),
  replicates: require('./replicates')(api, devices)
})
