const debug = require('debug')('cobox-server:broadcasts')
const assert = require('assert')
const pick = require('lodash.pick')
const { encodings, validators } = require('cobox-schemas')
const collect = require('collect-stream')

const Replicate = encodings.command.replicate
const Unreplicate = encodings.command.unreplicate
const isReplicate = validators.command.replicate
const isUnreplicate = validators.command.unreplicate

const resourceParams = require('../../../../../helpers/resource-params')
const { hex } = require('../../../../../../util')

const REPLICATE = 'command/replicate'
const UNREPLICATE = 'command/unreplicate'

const ReplicateCommandsController = module.exports = (api, devices) => {
  return {
    index,
    create,
    destroy
  }

  // -------------------- Controller Actions -------------------- //

  // GET /api/admin/devices/:id/commands/replicates
  async function index (params, opts = {}) {
    let device = await devices.show(resourceParams(params))

    return await new Promise((resolve, reject) => {
      const query = { $filter: { value: { type: { $in: [REPLICATE, UNREPLICATE] }, timestamp: { $gt: 0 } } } }
      const commands = device.log.read({ query: [query] })

      collect(commands, (err, msgs) => {
        if (err) return reject(err)
        return resolve(msgs)
      })
    })
  }

  // POST /api/admin/devices/:id/commands/replicate
  async function create (params, opts = {}) {
    let device = await devices.show(resourceParams(opts))

    let msg = {
      type: REPLICATE,
      timestamp: Date.now(),
      author: hex(api.profile.publicKey),
      content: resourceParams(params)
    }

    assert(isReplicate(msg), 'invalid parameters')

    await device.log.publish(Replicate.encode(msg))

    return msg
  }

  // POST /api/admin/devices/:id/commands/unreplicate
  async function destroy (params, opts = {}) {
    let device = await devices.show(resourceParams(opts))

    let msg = {
      type: UNREPLICATE,
      timestamp: Date.now(),
      author: hex(api.profile.publicKey),
      content: resourceParams(params)
    }

    assert(isUnreplicate(msg), 'invalid parameters')

    await device.log.publish(Unreplicate.encode(msg))

    return msg
  }
}
