const debug = require('debug')('cobox-server:admin-devices')
const assert = require('assert')
const pick = require('lodash.pick')
const SpaceDecorator = require('../../../../lib/decorators/space')
const { encodings } = require('cobox-schemas')
const { removeEmpty } =  require('../../../../util')
const { assign } = Object
const { isArray } = Array

const ConnectionsController = require('./connections')
const InvitesController = require('./invites')
const CommandsController = require('./commands')
const PeersController = require('./peers')

const DevicesController = require('../../devices')

const PeerAbout = encodings.peer.about
const SpaceAbout = encodings.space.about

const { hex } = require('../../../../util')

const AdminDevicesController = module.exports = (api) => {
  const AdminDevice = api.admin.devices.store
  const localDevices = DevicesController(api)

  const controller = {
    index,
    show,
    create,
    destroy
  }

  const children = {
    connections: ConnectionsController(api, controller),
    invites: InvitesController(api, controller),
    commands: CommandsController(api, controller),
    peers: PeersController(api, controller)
  }

  return assign(controller, children)

  // -------------------- Controller Actions -------------------- //

  // GET /api/admin/devices
  async function index (params, opts = {}) {
    let devices = await AdminDevice.all()
    if (!opts.decorate) return devices

    let response = devices
      .map(SpaceDecorator)
      .map((decorator) => decorator.toJSON())

    return response
  }

  // GET /api/admin/devices/:id
  async function show (params, opts = {}) {
    let device = await AdminDevice.findBy(deviceParams(params))
    if (!opts.decorate) return device
    let decorator = SpaceDecorator(device)
    return decorator.toJSON()
  }

  // POST /api/admin/devices
  async function create (params, opts = {}) {
    params = await handlePublicKey(params)

    let device = await AdminDevice.create(deviceParams(params))
    let decorator = SpaceDecorator(device)
    let commands = await handleCommands(params.commands, decorator.toJSON())

    await device.log.publish(PeerAbout.encode({
      type: 'peer/about',
      timestamp: Date.now(),
      author: hex(api.profile.publicKey),
      content: { name: api.profile.name }
    }))

    await device.log.publish(SpaceAbout.encode({
      type: 'space/about',
      timestamp: Date.now(),
      author: hex(api.profile.publicKey),
      content: { name: device.name }
    }))

    if (!opts.decorate) return device
    return removeEmpty(assign(decorator.toJSON(), { commands }))
  }

  // DELETE /api/admin/devices/:id
  async function destroy (params, opts = {}) {
    return false
  }

  // --------------------- Helpers ---------------------- //

  async function handleCommands (cmds, opts) {
    if (!isArray(cmds)) return

    const mappings = {
      announce: (params, opts) => children.commands.broadcasts.create(opts),
      hide: (params, opts) => children.commands.broadcasts.destroy(opts),
      replicate: children.commands.replicates.create,
      unreplicate: children.commands.replicates.destroy
    }

    let promises = cmds
      .filter((command) => mappings[command.action])
      .map((command) => mappings[command.action](command, opts))

    return await Promise.all(promises)
  }

  async function handlePublicKey (params) {
    if (!params.publicKey) return params
    let localDevice = await localDevices.show(params, { decorate: true })
    return assign({}, params, localDevice)
  }

  function deviceParams (params) {
    return pick(params, ['name', 'address', 'encryptionKey'])
  }
}
