const debug = require('debug')('cobox-server:admin/device/peers')
const assert = require('assert')
const collect = require('collect-stream')
const through = require('through2')

const SpaceDecorator = require('../../../../../lib/decorators/space')
const resourceParams = require('../../../../helpers/resource-params')
const { hex } = require('../../../../../util')

const ABOUT_PEER = 'peer/about'

const PeersController = module.exports = (api, devices) => {
  const controller = {
    index
  }

  return controller

  // GET /api/admin/devices/:id/peers
  async function index (params, opts = {}) {
    let device = await devices.show(resourceParams(params))

    return await new Promise((resolve, reject) => {
      let query = { $filter: { value: { type: ABOUT_PEER, timestamp: { $gt: 0 }} } }
      var stream = device.log.read({ query: [query] })
        .pipe(format('ADMIN_DEVICE', device))

      collect(stream, (err, msgs) => {
        if (err) return reject(err)
        return resolve(msgs)
      })
    })
  }
}

function format (resourceType, entry) {
  return through.obj(function (msg, _, next) {
    this.push({
      resourceType,
      feedId: msg.key,
      address: hex(entry.address),
      data: msg.value
    })
    next()
  })
}
