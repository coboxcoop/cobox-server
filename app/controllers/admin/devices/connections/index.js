const debug = require('debug')('cobox-server:admin/device/connections')
const assert = require('assert')
const collect = require('collect-stream')
const through = require('through2')

const SpaceDecorator = require('../../../../../lib/decorators/space')
const resourceParams = require('../../../../helpers/resource-params')
const { hex } = require('../../../../../util')

const ConnectionsController = module.exports = (api, devices) => {
  const AdminDevice = api.admin.devices.store

  const controller = {
    index,
    create,
    createAll,
    destroy,
    destroyAll
  }

  return controller

  // -------------------- Controller Actions -------------------- //

  // GET /api/admin/devices/:id/connections
  async function index (params, opts = {}) {
    let device = await devices.show(resourceParams(params))

    return await new Promise((resolve, reject) => {
      var connections = device.db.connections.createReadStream()
        .pipe(format('ADMIN_DEVICE', device))

      collect(connections, (err, msgs) => {
        if (err) return reject(err)
        return resolve(msgs)
      })
    })
  }

  // POST /api/admin/devices/:id/connections
  async function create (params, opts = {}) {
    let device = await devices.show(resourceParams(opts))

    device.swarm()

    await device.db.connections.batch([{
      type: 'put',
      key: hex(api.profile.publicKey),
      value: {
        type: 'peer/connection',
        timestamp: Date.now(),
        content: { connected: true }
      }
    }])

    if (!opts.decorate) return device
    let decorator = SpaceDecorator(device)
    return decorator.toJSON()
  }

  // POST /api/admin/devices/connections
  async function createAll (params, opts = {}) {
    let ds = await devices.index(resourceParams(opts))

    debug(`opening connections on`, ds.map((d) => hex(d.address)))

    await Promise.all(ds.map((device) => {
      return new Promise((resolve, reject) => {
        device.swarm()
        device.db.connections.batch([{
          type: 'put',
          key: hex(api.profile.publicKey),
          value: {
            type: 'peer/connection',
              timestamp: Date.now(),
              content: { connected: true }
            }
        }]).catch((err) => {
          if (err.code === 'ERR_ASSERTION') return resolve()
          reject(err)
        }).then(resolve)
      })
    }))

    if (!opts.decorate) return ds

    return ds
      .map(SpaceDecorator)
      .map((decorator) => decorator.toJSON())
  }

  // DELETE /api/admin/devices/:id/connections
  async function destroy (params, opts = {}) {
    let device = await devices.show(resourceParams(params))

    device.unswarm()

    await device.db.connections.batch([{
      type: 'put',
      key: hex(api.profile.publicKey),
      value: {
        type: 'peer/connection',
        timestamp: Date.now(),
        content: { connected: false }
      }
    }])

    if (!opts.decorate) return device
    let decorator = SpaceDecorator(device)
    return decorator.toJSON()
  }

  // DELETE /api/admin/devices/connections
  async function destroyAll (params, opts = {}) {
    let ds = await devices.index(resourceParams(opts))

    debug(`closing connections on`, ds.map((d) => hex(d.address)))

    await Promise.all(ds.map((device) => {
      return new Promise((resolve, reject) => {
        device.unswarm()
        device.db.connections.batch([{
          type: 'put',
          key: hex(api.profile.publicKey),
          value: {
            type: 'peer/connection',
              timestamp: Date.now(),
              content: { connected: false }
            }
        }]).catch((err) => {
          if (err.code === 'ERR_ASSERTION') return resolve()
          reject(err)
        }).then(resolve)
      })
    }))

    if (!opts.decorate) return ds

    return ds
      .map(SpaceDecorator)
      .map((d) => d.toJSON())
  }
}

function format (resourceType, entry) {
  return through.obj(function (msg, _, next) {
    var value = {
      resourceType,
      peerId: msg.key,
      address: hex(entry.address),
      data: msg.value
    }
    this.push(value)
    next()
  })
}
