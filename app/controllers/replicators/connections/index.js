const debug = require('debug')('cobox-server:replicator/connections')
const through = require('through2')
const assert = require('assert')
const collect = require('collect-stream')

const SpaceDecorator = require('../../../../lib/decorators/space')
const resourceParams = require('../../../helpers/resource-params')

const { hex } = require('../../../../util')

const ConnectionsController = module.exports = (api, replicators) => {
  const controller = {
    index,
    create,
    createAll,
    destroy,
    destroyAll
  }

  return controller

  // -------------------- Controller Actions -------------------- //

  // GET /api/replicators/:id/connections
  async function index (params, opts = {}) {
    let space = await replicators.show(resourceParams(params))

    return await new Promise((resolve, reject) => {
      var stream = replicator.db.connections.createReadStream()
        .pipe(format('REPLICATOR', replicator))

      collect(stream, (err, msgs) => {
        if (err) return reject(err)
        return resolve(msgs)
      })
    })
  }

  // POST /api/replicators/:id/connections
  async function create (params, opts = {}) {
    let replicator = await replicators.show(resourceParams(opts))

    debug(`opening connections on`, hex(replicator.address))

    replicator.swarm()

    await replicator.db.connections.batch([{
      type: 'put',
      key: hex(api.profile.publicKey),
      value: {
        type: 'peer/connection',
        timestamp: Date.now(),
        content: { connected: true }
      }
    }])

    if (!opts.decorate) return replicator
    let decorator = SpaceDecorator(replicator)
    return decorator.toJSON()
  }

  // POST /api/replicators/connections
  async function createAll (params, opts = {}) {
    let rs = await replicators.index(resourceParams(opts))

    debug(`opening connections on`, rs.map((r) => hex(r.address)))

    await Promise.all(rs.map((replicator) => {
      return new Promise((resolve, reject) => {
        replicator.swarm()
        replicator.db.connections.batch([{
          type: 'put',
          key: hex(api.profile.publicKey),
          value: {
            type: 'peer/connection',
              timestamp: Date.now(),
              content: { connected: true }
            }
        }]).catch((err) => {
          if (err.code === 'ERR_ASSERTION') return resolve()
          reject(err)
        }).then(resolve)
      })
    }))

    if (!opts.decorate) return rs
    let decorators = rs.map(SpaceDecorator)
    return decorators.map((d) => d.toJSON())
  }

  // DELETE /api/replicators/:id/connections
  async function destroy (params, opts = {}) {
    let replicator = await replicators.show(resourceParams(params))

    debug(`closing connections on`, hex(replicator.address))

    replicator.unswarm()

    await replicator.db.connections.batch([{
      type: 'put',
      key: hex(api.profile.publicKey),
      value: {
        type: 'peer/connection',
        timestamp: Date.now(),
        content: { connected: false }
      }
    }])

    if (!opts.decorate) return replicator
    let decorator = SpaceDecorator(replicator)
    return decorator.toJSON()
  }

  // DELETE /api/replicators/connections
  async function destroyAll (params, opts = {}) {
    let rs = await replicators.index(resourceParams(opts))

    debug(`closing connections on`, rs.map((r) => hex(r.address)))

    await Promise.all(rs.map((replicator) => {
      return new Promise((resolve, reject) => {
        replicator.unswarm()
        replicator.db.connections.batch([{
          type: 'put',
          key: hex(api.profile.publicKey),
          value: {
            type: 'peer/connection',
              timestamp: Date.now(),
              content: { connected: false }
            }
        }]).catch((err) => {
          if (err.code === 'ERR_ASSERTION') return resolve()
          reject(err)
        }).then(resolve)
      })
    }))

    if (!opts.decorate) return rs
    let decorators = rs.map(SpaceDecorator)
    return decorators.map((d) => d.toJSON())
  }
}

function format (resourceType, entry) {
  return through.obj(function (msg, _, next) {
    var value = {
      resourceType,
      feedId: msg.key,
      address: hex(entry.address),
      data: msg.value
    }
    this.push(value)
    next()
  })
}
