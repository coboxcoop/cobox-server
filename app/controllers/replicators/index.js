const assert = require('assert')
const resourceParams = require('../../helpers/resource-params')
const SpaceDecorator = require('../../../lib/decorators/space')
const ConnectionsController = require('./connections')
const { assign } = Object

const ReplicatorsController = module.exports = (api) => {
  const Replicator = api.replicators.store

  const controller = {
    index,
    show,
    create
  }

  const children = {
    connections: ConnectionsController(api, controller)
  }

  return assign(controller, children)

  // -------------------- Controller Actions -------------------- //

  // GET /api/replicators
  async function index (params, opts = {}) {
    let replicators = await Replicator.all()

    if (!opts.decorate) return replicators

    return replicators
      .map(SpaceDecorator)
      .map((decorator) => decorator.toJSON())
  }

  // GET /api/replicators/:id
  async function show (params, opts = {}) {
    let replicator = await Replicator.findBy(resourceParams(params))
    assert(replicator, 'does not exist')

    if (!opts.decorate) return replicator

    let decorator = SpaceDecorator(replicator)
    return decorator.toJSON()
  }

  // POST /api/replicators
  async function create (params, opts = {}) {
    let replicator = await Replicator.create(resourceParams(params))
    assert(replicator, 'failed to create')

    if (!opts.decorate) return replicators

    let decorator = SpaceDecorator(replicator)
    return decorator.toJSON()
  }

  // DELETE /api/replicators
  async function destroy (params, opts = {}) {
    throw new Error('route not yet implemented')
  }
}
