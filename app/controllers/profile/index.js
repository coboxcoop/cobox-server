const debug = require('debug')('cobox-server')
const crypto = require('cobox-crypto')
const ProfileDecorator = require('../../../lib/decorators/profile')

const ProfileController = module.exports = (api) => {
  const profile = api.profile

  const controller = {
    show,
    update
  }

  return controller

  // -------------------- Controller Actions -------------------- //

  // GET /api/profile
  async function show (params, opts = {}) {
    var decorator = ProfileDecorator(profile)
    return decorator.toJSON()
  }

  // PATCH /api/profile
  async function update (params, opts = {}) {
    profile.update(params)

    var decorator = ProfileDecorator(profile)
    return decorator.toJSON()
  }
}
