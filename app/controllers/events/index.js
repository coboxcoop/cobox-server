const debug = require('debug')('cobox-server:events')

module.exports = (api) => {
  return {
    live
  }

  // -------------------- Controller Actions -------------------- //

  // WS /api
  function live (params, opts = {}) {
    return api.events.stream()
  }
}
