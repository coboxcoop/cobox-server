const debug = require('debug')('cobox-server')
const path = require('path')
const { createPdf } = require('paper-key-backup')
const { loadKey } = require('cobox-keys')
const SpaceDecorator = require('../../../lib/decorators/space')

const PARENT_KEY = 'parent_key'
const BACKUP_FILE = 'key-backup.pdf'

const SpacesController = require('../spaces')

const KeysController = module.exports = (api) => {
  const storage = api.settings.storage
  const Space = api.spaces.store

  const controller = {
    export: exportFn,
    import: importFn
  }

  return controller

  // -------------------- Controller Actions -------------------- //

  // PUT /api/keys/export
  async function exportFn (params, opts = {}) {
    const parentKey = [{
      name: PARENT_KEY,
      data: loadKey(storage, PARENT_KEY)
    }]

    const spaces = await Space.all()

    var keys = spaces
      .map(SpaceDecorator)
      .map((decorator) => decorator.export())

    const filename = path.join(params.dir, BACKUP_FILE)

    return await new Promise((resolve, reject) => {
      createPdf(filename, parentKey.concat(keys), (err) => {
        if (err) reject(err)
        resolve({ filename })
      })
    })
  }

  // POST /api/keys/import
  async function importFn (params, opts = {}) {
    throw new Error('route not yet implemented')
  }
}
