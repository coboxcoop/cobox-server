const debug = require('debug')('cobox-server:space-drive')
const assert = require('assert')
const resourceParams = require('../../../helpers/resource-params')

const DriveController = module.exports = (api) => {
  const Space = api.spaces.store

  return {
    history,
    readdir,
    stat
  }
  // -------------------- Controller Actions -------------------- //

  // GET /api/spaces/:id/drive/history
  async function history (params, opts = {}) {
    let space = await Space.findBy(resourceParams(opts))
    assert(space, 'does not exist')

    await space.drive.ready()

    return await space.state.query(params.query)
  }

  // GET /api/spaces/:id/drive/readdir
  async function readdir (params, opts = {}) {
    let space = await Space.findBy(resourceParams(params))
    assert(space, 'does not exist')

    return await space.drive.ls(params.dir)
  }

  // GET /api/spaces/:id/drive/stat
  async function stat (params, opts = {}) {
    let space = await Space.findBy(resourceParams(params))
    assert(space, 'does not exist')

    await space.drive.ready()

    // TODO: this belongs in kappa-drive or cobox-space's drive handler
    // are there other useful fields we could supply here?
    return await new Promise((resolve, reject) => {
      const size = Object.values(space.drive._drives).reduce((acc, drive) => {
        const content = drive._contentStates.get(drive._db)
        acc += (drive.metadata.byteLength || 0) +
          (content.feed.byteLength || 0)
        return acc
      } , 0)

      return resolve({ size })
    })
  }
}
