const debug = require('debug')('cobox-server:space/peers')
const assert = require('assert')
const collect = require('collect-stream')
const through = require('through2')

const SpaceDecorator = require('../../../../lib/decorators/space')
const resourceParams = require('../../../helpers/resource-params')
const { hex } = require('../../../../util')

const ABOUT_PEER = 'peer/about'

const PeersController = module.exports = (api, spaces) => {
  const controller = {
    index
  }

  return controller

  // GET /api/spaces/:id/peers
  async function index (params, opts = {}) {
    let space = await spaces.show(resourceParams(params))

    return await new Promise((resolve, reject) => {
      let query = { $filter: { value: { type: ABOUT_PEER, timestamp: { $gt: 0 }} } }
      var stream = space.log.read({ query: [query] })
        .pipe(format('SPACE', space))

      collect(stream, (err, msgs) => {
        if (err) return reject(err)
        return resolve(msgs)
      })
    })
  }
}

function format (resourceType, entry) {
  return through.obj(function (msg, _, next) {
    this.push({
      resourceType,
      feedId: msg.key,
      address: hex(entry.address),
      data: msg.value
    })
    next()
  })
}
