const debug = require('debug')('cobox-server:admin/space/connections')
const assert = require('assert')
const collect = require('collect-stream')
const through = require('through2')

const SpaceDecorator = require('../../../../lib/decorators/space')
const resourceParams = require('../../../helpers/resource-params')
const { hex } = require('../../../../util')

const ConnectionsController = module.exports = (api, spaces) => {
  const Space = api.spaces.store

  const controller = {
    index,
    create,
    createAll,
    destroy,
    destroyAll
  }

  return controller

  // -------------------- Controller Actions -------------------- //

  // GET /api/spaces/:id/connections
  async function index (params, opts = {}) {
    let space = await spaces.show(resourceParams(params))

    return await new Promise((resolve, reject) => {
      var stream = space.db.connections.createReadStream()
        .pipe(format('SPACE', space))

      collect(stream, (err, msgs) => {
        if (err) return reject(err)
        return resolve(msgs)
      })
    })
  }

  // POST /api/spaces/:id/connections
  async function create (params, opts = {}) {
    let space = await spaces.show(resourceParams(opts))

    space.swarm()

    await space.db.connections.batch([{
      type: 'put',
      key: hex(api.profile.publicKey),
      value: {
        type: 'peer/connection',
        timestamp: Date.now(),
        content: { connected: true }
      }
    }])

    if (!opts.decorate) return space
    let decorator = SpaceDecorator(space)
    return decorator.toJSON()
  }

  // POST /api/spaces/connections
  async function createAll (params, opts = {}) {
    let gs = await spaces.index(resourceParams(opts))

    debug(`opening connections on`, gs.map((g) => hex(g.address)))

    await Promise.all(gs.map((space) => {
      return new Promise((resolve, reject) => {
        space.swarm()
        space.db.connections.batch([{
          type: 'put',
          key: hex(api.profile.publicKey),
          value: {
            type: 'peer/connection',
              timestamp: Date.now(),
              content: { connected: true }
            }
        }]).catch((err) => {
          if (err.code === 'ERR_ASSERTION') return resolve()
          reject(err)
        }).then(resolve)
      })
    }))

    if (!opts.decorate) return gs

    return gs
      .map(SpaceDecorator)
      .map((d) => d.toJSON())
  }

  // DELETE /api/spaces/:id/connections
  async function destroy (params, opts = {}) {
    let space = await spaces.show(resourceParams(params))

    space.unswarm()

    await space.db.connections.batch([{
      type: 'put',
      key: hex(api.profile.publicKey),
      value: {
        type: 'peer/connection',
        timestamp: Date.now(),
        content: { connected: false }
      }
    }])

    if (!opts.decorate) return space
    let decorator = SpaceDecorator(space)
    return decorator.toJSON()
  }

  // DELETE /api/spaces/connections
  async function destroyAll (params, opts = {}) {
    let gs = await spaces.index(resourceParams(opts))

    debug(`closing connections on`, gs.map((g) => hex(g.address)))

    await Promise.all(gs.map((space) => {
      return new Promise((resolve, reject) => {
        space.unswarm()
        space.db.connections.batch([{
          type: 'put',
          key: hex(api.profile.publicKey),
          value: {
            type: 'peer/connection',
              timestamp: Date.now(),
              content: { connected: false }
            }
        }]).catch((err) => {
          if (err.code === 'ERR_ASSERTION') return resolve()
          reject(err)
        }).then(resolve)
      })
    }))

    if (!opts.decorate) return gs

    return gs
      .map(SpaceDecorator)
      .map((d) => d.toJSON())
  }
}

function format (resourceType, entry) {
  return through.obj(function (msg, _, next) {
    var value = {
      resourceType,
      feedId: msg.key,
      address: hex(entry.address),
      data: msg.value
    }
    this.push(value)
    next()
  })
}
