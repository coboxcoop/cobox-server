const debug = require('debug')('cobox-server:space-mounts')
const assert = require('assert')
const spaceParams = require('../../../helpers/resource-params')
const SpaceDecorator = require('../../../../lib/decorators/space')
const { assign } = Object
const path = require('path')

const MountsController = module.exports = (api) => {
  const Space = api.spaces.store

  return {
    index,
    create,
    createAll,
    destroy,
    destroyAll
  }

  // -------------------- Controller Actions -------------------- //

  // GET /api/spaces/mounts
  async function index (params, opts = {}) {
    let spaces = await Space.all()
    let mounts = spaces.filter((space) => space.drive.location)

    if (!opts.decorate) return mounts

    return mounts
      .map(SpaceDecorator)
      .map((decorator) => decorator.toJSON())
  }

  // POST /api/spaces/:id/mounts
  async function create (params, opts = {}) {
    let space = await Space.findBy(spaceParams(opts))
    assert(space, 'does not exist')

    let location = path.join(params.location, space.name)
    await space.drive.mount(assign(params, { location }))

    let decorator = SpaceDecorator(space)
    return assign(decorator.toJSON(), { location })
  }

  // POST /api/spaces/mounts
  async function createAll (params, opts = {}) {
    let spaces = await Space.all()

    var mounts = spaces.filter((space) => !space.drive._unmount)

    mounts.forEach((space) => space.drive.mount({
      location: path.join(params.location, space.name)
    }))

    if (!opts.decorate) return mounts

    return mounts
      .map(SpaceDecorator)
      .map((d) => d.toJSON())
  }

  // DELETE /api/spaces/:id/mounts
  async function destroy (params = {}, opts) {
    let space = await Space.findBy(spaceParams(params))
    assert(space, 'does not exist')

    let location = await space.drive.unmount()

    let decorator = SpaceDecorator(space)
    return assign(decorator.toJSON(), { location })
  }

  // DELETE /api/spaces/mounts
  async function destroyAll (params, opts) {
    let spaces = await Space.all()

    var mounts = spaces.filter((space) => space.drive._unmount)
    mounts.forEach((space) => space.drive.unmount())

    if (!opts.decorate) return mounts

    return mounts
      .map(SpaceDecorator)
      .map((d) => d.toJSON())
  }
}
