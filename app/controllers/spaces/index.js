const debug = require('debug')('cobox-server:spaces')
const assert = require('assert')
const crypto = require('cobox-crypto')
const { encodings } = require('cobox-schemas')
const { assign } = Object

const ConnectionsController = require('./connections')
const MountsController = require('./mounts')
const DriveController = require('./drive')
const InvitesController = require('./invites')
const PeersController = require('./peers')

const SpaceDecorator = require('../../../lib/decorators/space')
const resourceParams = require('../../helpers/resource-params')

const { hex } = require('../../../util')

const PeerAbout = encodings.peer.about
const SpaceAbout = encodings.space.about

const SpacesController = module.exports = (api) => {
  const Space = api.spaces.store

  const controller = {
    index,
    show,
    create,
    destroy
  }

  const children = {
    connections: ConnectionsController(api, controller),
    mounts: MountsController(api, controller),
    drive: DriveController(api, controller),
    invites: InvitesController(api, controller),
    peers: PeersController(api, controller)
  }

  return assign(controller, children)

  // -------------------- Controller Actions -------------------- //

  // GET /api/spaces
  async function index (params, opts = {}) {
    let spaces = await Space.all()

    if (!opts.decorate) return spaces

    return spaces
      .map(SpaceDecorator)
      .map((decorator) => decorator.toJSON())
  }

  // GET /api/spaces/:id
  async function show (params, opts = {}) {
    let space = await Space.findBy(resourceParams(params))
    assert(space, 'does not exist')

    if (!opts.decorate) return space

    let decorator = SpaceDecorator(space)
    return decorator.toJSON()
  }

  // POST /api/spaces
  async function create (params, opts = {}) {
    if (params.code) return await children.invites.accept(params, opts)

    let space = await Space.create(createParams(params))

    assert(space, 'failed to create')

    await space.log.publish(PeerAbout.encode({
      type: 'peer/about',
      timestamp: Date.now(),
      author: hex(api.profile.publicKey),
      content: { name: api.profile.name }
    }))

    await space.log.publish(SpaceAbout.encode({
      type: 'space/about',
      timestamp: Date.now(),
      author: hex(api.profile.publicKey),
      content: { name: space.name }
    }))

    if (!opts.decorate) return space

    let decorator = SpaceDecorator(space)
    return decorator.toJSON()
  }

  // DELETE /api/spaces
  async function destroy (params, opts = {}) {
    return false
  }

  // --------------------- Helpers ---------------------- //

  function createParams (params) {
    var resParams = resourceParams(params)
    if (!params.address && !params.encryptionKey) {
      return assign({}, resParams, {
        address: hex(crypto.address()),
        encryptionKey: hex(crypto.encryptionKey())
      })
    }
    return resParams
  }
}
