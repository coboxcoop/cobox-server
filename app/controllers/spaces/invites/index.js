const debug = require('debug')('cobox-server')
const Invite = require('cobox-key-exchange')
const { loadKey } = require('cobox-keys')
const { encodings } = require('cobox-schemas')
const pick = require('lodash.pick')
const collect = require('collect-stream')

const SpaceDecorator = require('../../../../lib/decorators/space')
const resourceParams = require('../../../helpers/resource-params')
const SpacesController = require('../')
const ConnectionsController = require('../connections')
const { hex } = require('../../../../util')

const { assign } = Object
const PeerInvite = encodings.peer.invite
const PeerAbout = encodings.peer.about

const InvitesController = module.exports = (api, spaces) => {
  const controller = {
    accept,
    index,
    create
  }

  return controller

  // -------------------- Controller Actions -------------------- //

  // GET /api/spaces/invites/accept?code=
  async function accept (params, opts = {}) {
    let spaceParams

    try {
      spaceParams = Invite.open(Buffer.from(params.code, 'hex'), api.profile.identity)
    } catch (error) {
      throw new Error('invite code invalid')
    }

    let space = await spaces.create(spaceParams)
    var decorator = SpaceDecorator(space)
    var _opts = assign({}, opts, resourceParams(decorator.toJSON()))
    return await spaces.connections.create(null, _opts)
  }

  // GET /api/spaces/:id/invites
  async function index (params, opts = {}) {
    let space = await spaces.show(resourceParams(params))

    return await new Promise((resolve, reject) => {
      const query = { $filter: { value: { type: 'peer/invite', timestamp: { $gt: 0 } } } }
      const invites = space.log.read({ query: [query] })

      collect(invites, (err, msgs) => {
        if (err) return reject(err)
        return resolve(msgs)
      })
    })
  }

  // POST /api/spaces/:id/invites
  async function create (params, opts = {}) {
    let space = await spaces.show(resourceParams(opts))
    let code = Invite.create(inviteParams(space, params))

    let msg = {
      type: 'peer/invite',
      timestamp: Date.now(),
      author: hex(api.profile.publicKey),
      content: {
        code: hex(code),
        publicKey: hex(params.publicKey)
      }
    }

    await space.log.publish(PeerInvite.encode(msg))

    return msg
  }
}

function inviteParams (space, params) {
  var encryptionKey = loadKey(space.path, 'encryption_key')
  var spaceParams = assign(SpaceDecorator(space).toJSON(), params)
  var picked = pick(spaceParams, ['publicKey', 'address', 'name'])
  return assign({ encryptionKey }, picked)
}
