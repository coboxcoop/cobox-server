const express = require('express')

const AdminController = require('./admin')
const DevicesController = require('./devices')
const EventsController = require('./events')
const SpacesController = require('./spaces')
const KeysController = require('./keys')
const ReplicatorsController = require('./replicators')
const ProfileController = require('./profile')
const SystemController = require('./system')

const ApplicationController = module.exports = (api) => ({
  admin: AdminController(api),
  devices: DevicesController(api),
  events: EventsController(api),
  spaces: SpacesController(api),
  keys: KeysController(api),
  profile: ProfileController(api),
  replicators: ReplicatorsController(api),
  system: SystemController(api)
})
