const AdminSpace = require('cobox-admin-group')
const { assign } = Object

module.exports = function (storage, opts) {
  return function (params) {
    return AdminSpace(
      storage,
      params.address,
      assign({}, params, opts)
    )
  }
}
