const Space = require('cobox-group')
const { assign } = Object

module.exports = function (storage, opts) {
  return function (params) {
    return Space(
      storage,
      params.address,
      assign({}, params, opts)
    )
  }
}
