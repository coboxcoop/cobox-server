const YAML = require('js-yaml')
const path = require('path')
const fs = require('fs')
const crypto = require('cobox-crypto')
const sodium = require('sodium-native')
const { loadKey, saveKey } = require('cobox-keys')
const constants = require('cobox-constants')
const assert = require('assert')
const { hex } = require('../../util')
const { assign } = Object

const { keyIds } = constants
const PROFILE = 'profile.yml'
const PARENT_KEY = 'parent_key'

class Profile {
  constructor (storage) {
    this.root = storage
    this.storage = path.join(this.root, PROFILE)

    let identity = { name: null }

    const parentKey = loadKey(this.root, PARENT_KEY) || crypto.masterKey()
    saveKey(this.root, PARENT_KEY, parentKey)
    const keyPair = crypto.deriveBoxKeyPair(parentKey, keyIds.identity)
    sodium.sodium_memzero(parentKey)

    if (fs.existsSync(this.storage)) {
      identity = YAML.safeLoad(fs.readFileSync(this.storage))
    } else {
      fs.writeFileSync(this.storage, YAML.safeDump(assign(identity, {
        publicKey: hex(keyPair.publicKey),
      }), { sortKeys: true }))
    }

    this._name = identity.name
    this._publicKey = keyPair.publicKey
    this._secretKey = keyPair.secretKey
  }

  get name () {
    return this._name
  }

  set name (value) {
    this._name = value
    return this.name
  }

  get publicKey () {
    return this._publicKey
  }

  get secretKey () {
    return this._secretKey
  }

  get identity () {
    return {
      publicKey: hex(this.publicKey),
      secretKey: hex(this.secretKey),
    }
  }

  update (params) {
    assert(params.name, 'invalid: name required')
    this._name = params.name

    const data = {
      name: this.name,
      publicKey: hex(this.publicKey)
    }

    fs.writeFileSync(this.storage, YAML.safeDump(data, { sortKeys: true }))

    return data
  }
}

module.exports = (storage) => new Profile(storage)
module.exports.Profile = Profile
