const Replicator = require('cobox-replicator')
const { assign } = Object

module.exports = function (storage, opts) {
  return function (params) {
    return Replicator(
      storage,
      params.address,
      assign({}, params, opts)
    )
  }
}
