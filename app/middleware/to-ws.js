const debug = require('debug')('cobox-server')
const websocketStream = require('websocket-stream/stream')
const { assign } = Object
const jsonify = require('../helpers/jsonify')

module.exports = function toWS (fn) {
  return async function (ws, req) {
    let opts = assign({}, req.query, req.params)

    fn(opts)
      .pipe(jsonify())
      .pipe(websocketStream(ws, {
        objectMode: true
      }))
  }
}
