const debug = require('debug')('cobox-server')
const isEmpty = require('lodash.isempty')
const { validationResult } = require('express-validator')
const { removeEmpty, isHexString } = require('../../util')

module.exports = function toHTTP (fn) {
  return async function (req, res) {
    const errors = validationResult(req)

    if (!errors.isEmpty()) {
      return res.status(422).json({
        errors: errors.array()
      })
    }

    try {
      let obj = req.body
      let opts = Object.assign({}, req.query, req.params)

      // handle names or addresses as params.id
      if (opts.id) {
        if (isHexString(opts.id, 64)) opts.address = opts.id
        else opts.name = opts.id
      }

      // if this is a GET, or a DELETE, req.body should be null
      // if this is a PUT, POST, or PATCH, req.body is an object
      if (isEmpty(obj)) {
        obj = opts
        opts = {}
      }

      opts.decorate = true

      let data = await fn(obj, opts)

      return res
        .status(200)
        .json(data)

    } catch (error) {
      debug(error)

      // TODO: handle other issues, isn't always a 404
      return res
        .status(404)
        .json({
          errors: [{
            value: error.name,
            msg: error.message
          }]
        })
    }
  }
}
