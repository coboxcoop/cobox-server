const through = require('through2')

module.exports = function jsonify () {
  return through.obj(function (msg, enc, next) {
    if (msg.sync) return next()
    this.push(JSON.stringify(msg))
    next()
  })
}
