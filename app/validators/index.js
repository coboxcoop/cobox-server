const AdminValidator = require('./admin')
const SpacesValidator = require('./spaces')
const InviteValidator = require('./invites')
const KeysValidator = require('./keys')
const ReplicatorsValidator = require('./replicators')
const ProfileValidator = require('./profile')

const Validators = module.exports = (api) => ({
  admin: AdminValidator(api),
  spaces: SpacesValidator(api),
  invites: InviteValidator(api),
  keys: KeysValidator(api),
  profile: ProfileValidator(api),
  replicators: ReplicatorsValidator(api)
})
