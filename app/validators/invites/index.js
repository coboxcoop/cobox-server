const debug = require('debug')('cobox-server')
const crypto = require('cobox-crypto')
const { body, query } = require('express-validator')
const { definitions } = require('cobox-schemas')

const { publicKeyChain } = require('../chains')

module.exports = (api) => ({
  accept: [
    query('code')
      .exists()
      .withMessage('\'code\' required')
      .bail()
      .matches(definitions.inviteCode.pattern)
      .withMessage('\'code\' invalid')
      .bail()
  ],
  create: [
    publicKeyChain()
  ]
})
