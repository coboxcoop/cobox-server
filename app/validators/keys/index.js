const { body } = require('express-validator')
const os = require('os')
const path = require('path')

module.exports = (api) => ({
  export: [
    body('dir')
      .customSanitizer((dir, { req }) => dir || os.homedir())
      .not().isInt()
      .bail().withMessage('\'dir\' must be a string')
      .custom((dir) => path.isAbsolute(dir)).withMessage('\'dir\' must be a path')
  ]
})
