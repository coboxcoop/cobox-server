const { body } = require('express-validator')
const crypto = require('cobox-crypto')
const assert = require('assert')

module.exports = (api) => ({
  create: [
    nameChain(),
    addressChain()
  ],
  destroy: [
    nameChain(),
    addressChain()
  ]
})

function nameChain () {
  return body('name')
    .not()
    .isEmpty()
    .withMessage('\'name\' required')
    .bail()
    .custom((name) => !crypto.isKey(name))
    .withMessage('\'name\' must be below 32 characters')
    .bail()
}

function addressChain () {
  return body('address')
    .not()
    .isEmpty()
    .withMessage('\'address\' required')
    .bail()
    .custom((address) => crypto.isKey(address))
    .withMessage('\'address\' must be a 32 byte address')
    .bail()
}
