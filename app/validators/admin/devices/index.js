const { body, oneOf } = require('express-validator')
const assert = require('assert')
const crypto = require('cobox-crypto')

const {
  encryptionKeyChain,
  nestedCommandsChain,
  publicKeyChain,
  SpaceByNameChain,
  SpaceByAddressChain
} = require('../../chains')

module.exports = (api) => {
  const adminDeviceByNameChain = SpaceByNameChain(api.admin.devices.store)
  const adminDeviceByAddressChain = SpaceByAddressChain(api.admin.devices.store)

  return {
    create: [
      adminDeviceByNameChain(),
      oneOf([
        [
          adminDeviceByAddressChain(),
          encryptionKeyChain()
        ],
        [
          publicKeyChain()
        ]
      ]),
      nestedCommandsChain()
    ],
    commands: require('./commands')(api)
  }
}
