const path = require('path')
const { body } = require('express-validator')

module.exports = (api) => ({
  create: [
    body('location')
      .customSanitizer((location) => location || api.settings.mount)
  ]
})
