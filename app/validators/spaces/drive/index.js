const path = require('path')
const { body } = require('express-validator')

module.exports = (api) => ({
  history: [
    body('query')
      .customSanitizer((query, { req }) => (
        query || { $filter: { value: { timestamp: { $gt: 0 } } } }
      ))
  ],

  readdir: [
    body('dir')
      .customSanitizer((dir) => dir || '/')
      .not()
      .isInt()
      .bail()
      .withMessage('\'dir\' must be a string')
      .custom((dir) => path.isAbsolute(dir))
      .withMessage('\'dir\' must be a path')
  ]
})
