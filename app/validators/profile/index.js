const { body } = require('express-validator')
const crypto = require('cobox-crypto')

module.exports = (api) => ({
  update: [
    body('name')
      .not()
      .isEmpty()
      .withMessage('\'name\' required')
      .custom((name) => !crypto.isKey(name))
      .withMessage('\name\' must be below 32 characters')
  ]
})
