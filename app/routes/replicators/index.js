const express = require('express')
const expressWs = require('express-ws')
const toHTTP = require('../../middleware/to-http')

const ReplicatorRoutes = module.exports = ({ controllers, validators }) => {
  const router = express.Router()
  expressWs(router)

  router.get('/', toHTTP(controllers.replicators.index))
  router.post('/', validators.replicators.create, toHTTP(controllers.replicators.create))
  router.get('/:id', toHTTP(controllers.replicators.show))

  router.get('/:id/connections', toHTTP(controllers.replicators.connections.index))
  router.post('/:id/connections', toHTTP(controllers.replicators.connections.create))
  router.delete('/:id/connections', toHTTP(controllers.replicators.connections.destroy))
  router.post('/connections', toHTTP(controllers.replicators.connections.createAll))
  router.delete('/connections', toHTTP(controllers.replicators.connections.destroyAll))

  return router
}
