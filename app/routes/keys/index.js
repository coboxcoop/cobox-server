const express = require('express')
const expressWs = require('express-ws')
const toHTTP = require('../../middleware/to-http')

const KeyRoutes = module.exports = ({ controllers, validators }) => {
  const router = express.Router()
  expressWs(router)

  router.put('/export', validators.keys.export, toHTTP(controllers.keys.export))

  return router
}
