const express = require('express')
const expressWs = require('express-ws')
const toWS = require('../../middleware/to-ws')

const EventRoutes = module.exports = ({ controllers, validators }) => {
  const router = express.Router()
  expressWs(router)

  router.ws('/', toWS(controllers.events.live))

  return router
}

