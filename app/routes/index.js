const express = require('express')
const expressWs = require('express-ws')
const toHTTP = require('../middleware/to-http')
const debug = require('debug')('cobox-server')
const capture = require('capture-exit')

const ApplicationController = require('../controllers')
const ApplicationValidator = require('../validators')

module.exports = function Routes (api) {
  const shutdown = api.shutdown
  const router = express.Router()
  expressWs(router)

  const app = {
    controllers: ApplicationController(api),
    validators: ApplicationValidator(api)
  }

  router.use('/', require('./events')(app))
  router.use('/admin', require('./admin')(app))
  router.use('/devices', require('./devices')(app))
  router.use('/spaces', require('./spaces')(app))
  router.use('/keys', require('./keys')(app))
  router.use('/profile', require('./profile')(app))
  router.use('/replicators', require('./replicators')(app))
  router.use('/system', require('./system')(app))

  router.get('/system/routes', (req, res) => {
    let routes = app.controllers.system.routes({ router })

    return res
      .status(200)
      .json(routes)
  })

  router.get('/stop', (req, res) => {
    api.shutdown()
      .then((done) => {
        res.status(200)
        return done()
      }).catch((err, done) => {
        debug(err)
        res.status(500)
        return done()
      })
  })

  return router
}
