const express = require('express')
const expressWs = require('express-ws')
const toHTTP = require('../../middleware/to-http')

module.exports = function Routes ({ controllers, validators }) {
  const router = express.Router()
  expressWs(router)

  router.get('/', toHTTP(controllers.spaces.index))
  router.post('/', validators.spaces.create, toHTTP(controllers.spaces.create))
  router.get('/mounts', toHTTP(controllers.spaces.mounts.index))
  router.get('/invites/accept', validators.invites.accept, toHTTP(controllers.spaces.invites.accept))

  router.get('/:id', toHTTP(controllers.spaces.show))
  router.get('/:id/peers', toHTTP(controllers.spaces.peers.index))

  router.get('/:id/invites', toHTTP(controllers.spaces.invites.index))
  router.post('/:id/invites', validators.invites.create, toHTTP(controllers.spaces.invites.create))

  router.post('/mounts', validators.spaces.mounts.create, toHTTP(controllers.spaces.mounts.createAll))
  router.delete('/mounts', toHTTP(controllers.spaces.mounts.destroyAll))
  router.post('/:id/mounts', validators.spaces.mounts.create, toHTTP(controllers.spaces.mounts.create))
  router.delete('/:id/mounts', toHTTP(controllers.spaces.mounts.destroy))

  router.get('/:id/connections', toHTTP(controllers.spaces.connections.index))
  router.post('/:id/connections', toHTTP(controllers.spaces.connections.create))
  router.delete('/:id/connections', toHTTP(controllers.spaces.connections.destroy))
  router.post('/connections', toHTTP(controllers.spaces.connections.createAll))
  router.delete('/connections', toHTTP(controllers.spaces.connections.destroyAll))

  router.get('/:id/drive/history', validators.spaces.drive.history, toHTTP(controllers.spaces.drive.history))
  router.get('/:id/drive/stat', toHTTP(controllers.spaces.drive.stat))
  router.get('/:id/drive/readdir', toHTTP(controllers.spaces.drive.readdir))

  return router
}
