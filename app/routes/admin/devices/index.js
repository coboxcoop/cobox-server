const express = require('express')
const expressWs = require('express-ws')
const toHTTP = require('../../../middleware/to-http')

const AdminDeviceRoutes = module.exports = ({ controllers, validators, helpers }) => {
  const router = express.Router()
  expressWs(router)

  router.get('/', toHTTP(controllers.admin.devices.index))
  router.get('/:id', toHTTP(controllers.admin.devices.show))
  router.post('/', validators.admin.devices.create, toHTTP(controllers.admin.devices.create))
  router.get('/:id/peers', toHTTP(controllers.admin.devices.peers.index))

  router.get('/:id/connections', toHTTP(controllers.admin.devices.connections.index))
  router.post('/:id/connections/', toHTTP(controllers.admin.devices.connections.create))
  router.delete('/:id/connections/', toHTTP(controllers.admin.devices.connections.destroy))
  router.post('/connections', toHTTP(controllers.admin.devices.connections.createAll))
  router.delete('/connections', toHTTP(controllers.admin.devices.connections.destroyAll))

  router.get('/invites/accept', validators.invites.accept, toHTTP(controllers.admin.devices.invites.accept))
  router.get('/:id/invites', toHTTP(controllers.admin.devices.invites.index))
  router.post('/:id/invites/', validators.invites.create, toHTTP(controllers.admin.devices.invites.create))

  // TODO: make these RESTful (POST/DELETE)
  router.patch('/:id/commands/broadcast', toHTTP(controllers.admin.devices.commands.broadcasts.update))
  router.post('/:id/commands/announce', toHTTP(controllers.admin.devices.commands.broadcasts.create))
  router.post('/:id/commands/hide', toHTTP(controllers.admin.devices.commands.broadcasts.destroy))

  // TODO: make these RESTful (POST/DELETE)
  router.get('/:id/commands/replicates', toHTTP(controllers.admin.devices.commands.replicates.index))
  router.post('/:id/commands/replicate', validators.admin.devices.commands.replicates.create, toHTTP(controllers.admin.devices.commands.replicates.create))
  router.post('/:id/commands/unreplicate', validators.admin.devices.commands.replicates.destroy, toHTTP(controllers.admin.devices.commands.replicates.destroy))

  return router
}

