const express = require('express')
const expressWs = require('express-ws')
const toHTTP = require('../../middleware/to-http')

module.exports = (app) =>  {
  const router = express.Router()
  expressWs(router)

  router.use('/devices', require('./devices')(app))

  return router
}
