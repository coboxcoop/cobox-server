const express = require('express')
const expressWs = require('express-ws')
const toHTTP = require('../../middleware/to-http')

const KeyRoutes = module.exports = ({ controllers, validators }) => {
  const router = express.Router()
  expressWs(router)

  router.get('/', toHTTP(controllers.profile.show))
  router.patch('/', validators.profile.update, toHTTP(controllers.profile.update))

  return router
}
