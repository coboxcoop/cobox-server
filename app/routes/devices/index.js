const express = require('express')
const expressWs = require('express-ws')
const toWS = require('../../middleware/to-ws')
const toHTTP = require('../../middleware/to-http')

const EventRoutes = module.exports = ({ controllers, validators }) => {
  const router = express.Router()
  expressWs(router)

  router.ws('/', toWS(controllers.devices.live))
  router.get('/', toHTTP(controllers.devices.index))
  router.get('/:id', toHTTP(controllers.devices.show))

  return router
}

