const express = require('express')
const expressWs = require('express-ws')
const bodyParser = require('body-parser')
const swaggerUI = require('swagger-ui-express')
const constants = require('cobox-constants')
const mkdirp = require('mkdirp')
const path = require('path')
const cors = require('cors')
const capture = require('capture-exit')

capture.captureExit()

const start = require('./lib/start')
const close = require('./lib/close')
const swaggerDocs = require('./docs/swagger.json')
const Routes = require('./app/routes')
const Profile = require('./app/models/profile')
const Logger = require('./lib/logger')
const logfile = [new Date().toISOString().replace(/:/g, '-').replace(/\./, '-'), '.json'].join('')

const SpaceStore = require('./lib/spaces')
const AdminDeviceStore = require('./lib/admin/devices')
const ReplicatorStore = require('./lib/replicators')
const DeviceStore = require('./lib/devices')
const EventStore = require('./lib/events')

const { untilde } = require('./util')

function Application (opts = {}) {
  const udpPort = opts.udpPort || 8999
  const mount = opts.mount = untilde(opts.mount) || constants.mount
  const storage = opts.storage = untilde(opts.storage) || constants.storage
  const logger = Logger(path.join(storage, 'logs', logfile))

  mkdirp.sync(mount)

  const app = express()
  expressWs(app)

  app.use(express.static(path.join(__dirname, 'public')))
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(bodyParser.json())
  app.use(cors())

  const identity = Profile(storage)
  const spaces = SpaceStore(storage, { identity })
  const adminDevices = AdminDeviceStore(storage, { identity })
  const replicators = ReplicatorStore(storage)

  const shutdown = Shutdown(app)
  const api = {
    spaces,
    replicators,
    admin: { devices: adminDevices },
    devices: DeviceStore({ port: udpPort }),
    events: EventStore([spaces, replicators, adminDevices]),
    settings: { storage, mount },
    profile: identity,
    system: require('./package.json'),
    logger,
    shutdown
  }

  var router = Routes(api)
  app.use('/api', router)
  app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocs))
  app.set('api', api)

  app.start = api.start = start(app, opts)
  app.close = api.close = close(app, opts)

  capture.onExit(shutdown)

  function Shutdown (app) {
    return function gracefulShutdown () {
      return new Promise((resolve, reject) => {
        app.close((err, done) => {
          if (err) return reject(err, done)
          resolve(done)
        })
      })
    }
  }

  return app
}

module.exports = Application
