const thunky = require('thunky')
const debug = require('debug')('cobox-server')
const rimraf = require('rimraf')
const capture = require('capture-exit')

module.exports = function close (app, opts) {
  const api = app.get('api')

  return thunky(function (cb = noop) {
    let pending = 3

    api.spaces.store.close(next)
    api.replicators.store.close(next)
    api.admin.devices.store.close(next)

    function next (err) {
      if (err) {
        pending = Infinity
        return cb(err)
      }
      if (!--pending) return cb(null, done)
    }

    function done () {
      app.server.forceShutdown((err) => {
        debug('closed: server', err || '' )
        var mount = app.get('mount')
        rimraf(mount, (err) => {
          debug(`unmounted ${mount}`, err ||  '')
          console.log('\n...by block by block, blocks all the way down')
          capture.offExit(api.shutdown)
          capture.releaseExit()
          return process.exit(1)
        })
      })
    }
  })
}

