const memdb = require('level-mem')
const collect = require('collect-stream')
const createBroadcastStream = require('broadcast-stream')
const through = require('through2')
const debug = require('debug')('cobox-server')
const LiveStream = require('level-live-stream')
const { encodings, validators } = require('cobox-schemas')
const Broadcast = encodings.devices.broadcast
const isBroadcast = validators.devices.broadcast

module.exports = (opts = {}) => {
  var source = createBroadcastStream(opts.port)
    .pipe(decode())

  return {
    store: DeviceStore(source)
  }
}

function decode () {
  return through.obj(function (payload, enc, next) {
    try { var msg = Broadcast.decode(payload) }
    catch (err) { return next() }
    if (!isBroadcast(msg)) return next()
    debug(msg)
    this.push(msg)
    next()
  })
}

function DeviceStore (stream, opts = {}) {
  const interval = opts.interval || 1000
  const db = memdb({ valueEncoding: Broadcast })

  stream.pipe(through.obj(function (msg, enc, next) {
    db.put(msg.author, msg, next)
  }))

  // TODO: using setInterval like this is a hack
  // this will not work in the long term, any disruption to
  // node's event loop and this will fail to continue running
  // this should be handled by an out-sourced worker process
  setInterval(() => {
    var now = Date.now()
    var devices = db.createReadStream()

    collect(devices, (err, msgs) => {
      var ops = msgs
        .map((msg) => msg.value)
        .filter((value) => value.content.broadcasting)
        .filter((value) => (now - interval > value.timestamp))
        .map((value) => {
          value.content.broadcasting = false
          return value
        })
        .map((value) => ({
          type: 'put',
          key: value.author,
          value
        }))

      db.batch(ops)
    })
  }, interval)

  LiveStream.install(db)

  return db
}
