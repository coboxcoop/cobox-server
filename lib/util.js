const debug = require('debug')('cobox-server')
const SpaceDecorator = require('./decorators/space')

function logResource (entry) {
  var decorator = SpaceDecorator(entry)
  var resource = decorator.toJSON({ secure: true })
  debug(`loaded ${entry.constructor.name}: ${JSON.stringify(resource, null, 2)}`)
}

module.exports = {
  logResource
}
