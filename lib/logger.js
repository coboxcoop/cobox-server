const pino = require('pino')

module.exports = function (storage, opts = {}) {
  return function (name, suboptions = {}) {
    if (process.env.NODE_ENV !== 'production') return noop
    const logger = pino({ name }, pino.destination(storage))
    return function (...args) {
      if (opts.stdout || suboptions.stdout) console.log(...args)
      logger.info(...args)
    }
  }
}

function noop () {}
