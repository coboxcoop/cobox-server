const crypto = require('cobox-crypto')
const path = require('path')
const Repository = require('@coboxcoop/repository')
const Space = require('../app/models/space')
const { deriveFromStoredParent } = require('cobox-keys')
const { isDevelopment } = require('../util')
const { logResource } = require('./util')
const { assign } = Object

module.exports = (storage, opts = {}) => {
  const namespace = path.join(storage, 'spaces')
  const deriveKeyPair = deriveFromStoredParent(storage, crypto.keyPair)
  const createSpace = Space(namespace, assign({ deriveKeyPair, opts }))

  const repository = Repository(namespace, createSpace)
  if (isDevelopment()) repository.on('entry', logResource)

  return { store: repository }
}
