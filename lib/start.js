const thunky = require('thunky')
const debug = require('debug')('cobox-server')
const shutdown = require('http-shutdown')

module.exports = function start (app, opts = {}) {
  const api = app.get('api')

  return thunky(function (cb = noop) {
    console.log("stacking it up, block by block by...")
    let pending = 3

    api.spaces.store.ready(next)
    api.replicators.store.ready(next)
    api.admin.devices.store.ready(next)

    function next (err) {
      if (err) {
        pending = Infinity
        return cb(err)
      }
      if (--pending === 0) return done()
    }

    function done () {
      app.set('hostname', opts.hostname)
      app.set('port', opts.port)
      app.set('udpPort', opts.udpPort)
      app.set('storage', opts.storage)
      app.set('mount', opts.mount)

      app.server = app.listen(opts.port, opts.hostname, cb)

      shutdown(app.server)
      return cb()
    }
  })
}

function noop () {}
