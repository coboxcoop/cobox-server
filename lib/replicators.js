const path = require('path')
const debug = require('debug')('cobox-server')
const Repository = require('@coboxcoop/repository')
const Replicator = require('../app/models/replicator')
const { isDevelopment } = require('../util')
const { logResource } = require('./util')

module.exports = (storage, opts = {}) => {
  const namespace = path.join(storage, 'replicators')
  const createReplicator = Replicator(namespace, opts)

  const repository = Repository(namespace, createReplicator)
  if (isDevelopment()) repository.on('entry', logResource)

  return { store: repository }
}
