const crypto = require('cobox-crypto')
const path = require('path')
const Repository = require('@coboxcoop/repository')
const AdminSpace = require('../../app/models/admin/device')
const { deriveFromStoredParent } = require('cobox-keys')
const { isDevelopment } = require('../../util')
const { logResource } = require('../util')
const { assign } = Object

module.exports = (storage, opts = {}) => {
  const namespace = path.join(storage, 'devices')
  const deriveKeyPair = deriveFromStoredParent(storage, crypto.keyPair)
  const createAdminSpace = AdminSpace(namespace, assign({ deriveKeyPair, opts }))

  const repository = Repository(namespace, createAdminSpace)

  if (isDevelopment()) repository.on('entry', logResource)

  return { store: repository }
}
