module.exports = process.env.DEVELOPMENT
  ? require('debug')('cobox-server')
  : console.log
