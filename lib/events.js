const debug = require('debug')('cobox-server:events')
const merge = require('merge-stream')
const { isFunction } = require('util')

class EventStore {
  constructor (sources, opts = {}) {
    this.sources = sources
    this.opts = opts
  }

  createReadStream () {
    const stream = merge()
    this.sources.forEach(addSource)
    return stream

    function addSource (source) {
      source.on('entry', (entry) => {
        if (isFunction(entry.createConnectionsStream)) stream.add(entry.createConnectionsStream())
        if (isFunction(entry.createLogStream)) stream.add(entry.createLogStream())
      })
    }
  }
}

module.exports = (sources) => new EventStore(sources)
