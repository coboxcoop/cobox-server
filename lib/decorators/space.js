const debug = require('debug')('cobox-server')
const { hex, removeEmpty } = require('../../util')
const { loadKey } = require('cobox-keys')

const SpaceDecorator = module.exports = (space) => ({
  toJSON: (opts = {}) => removeEmpty({
    name: space.name,
    address: hex(space.address),
    discoveryKey: space.discoveryKey ? hex(space.discoveryKey) : null,
    encryptionKey: opts.secure ? hex(loadKey(space.path, 'encryption_key')) : null,
    location: space.drive ? space.drive.location : null,
    size: space.bytesUsed()
  }),
  export: () => ({
    name: `***Space***: ${space.name}`,
    data: hex(loadKey(space.path, 'encryption_key')),
    comment: `***Address***: ${hex(space.address)}`
  })
})
