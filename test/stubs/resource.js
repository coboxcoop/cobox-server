const sinon = require('sinon')
const { assign } = Object

module.exports = function stubResource (storage, stubs) {
  return function Resource (params) {
    var entry = assign({}, params, stubs, {
      attributes: params,
      path: storage,
      bytesUsed: sinon.stub()
    })
    return entry
  }
}
