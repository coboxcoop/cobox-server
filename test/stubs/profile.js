const crypto = require('cobox-crypto')
const randomWords = require('random-words')
const sinon = require('sinon')
const { assign } = Object

module.exports = function Profile (stubs = {}) {
  const keyPair = crypto.boxKeyPair()

  const identity = {
    name: randomWords(1).pop(),
    publicKey: Buffer.from(keyPair.publicKey),
    secretKey: Buffer.from(keyPair.secretKey)
  }

  return assign({}, identity, {
    identity,
    update: sinon.stub().returns(identity)
  }, stubs)
}
