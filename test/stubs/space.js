const crypto = require('cobox-crypto')
const sinon = require('sinon')

const SpaceStubs = module.exports = () => ({
  ready: sinon.stub().callsArg(0),
  swarm: sinon.stub(),
  unswarm: sinon.stub(),
  log: {
    ready: sinon.stub().callsArg(0),
    publish: sinon.stub().resolves()
  },
  drive: {
    ready: sinon.stub().resolves(0),
    ls: sinon.stub().resolves(require('../fixtures/ls.json')),
    mount: sinon.stub().resolves(true),
    unmount: sinon.stub().resolves(true)
  },
  state: {
    ready: sinon.stub().resolves(),
    query: sinon.stub().returns(require('../fixtures/state.json'))
  },
  bytesUsed: sinon.stub()
})
