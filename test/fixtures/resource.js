const crypto = require('cobox-crypto')
const randomWords = require('random-words')
const { hex } = require('../../util')

module.exports = function seedResource () {
  const resources = Array
    .from(new Array(Math.floor(1 + Math.random() * Math.floor(9))).keys())
    .reduce((acc, i) => {
      var address = hex(crypto.address()),
        name = randomWords(1).pop()
      acc[address] = { name, address }
      return acc
    }, {})

  return resources
}
