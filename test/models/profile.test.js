const { describe } = require('tape-plus')
const crypto = require('cobox-crypto')
const proxyquire = require('proxyquire')
const sinon = require('sinon')
const Profile = require('../../app/models/profile')
const randomWords = require('random-words')
const { cleanup, tmp } = require('../util')
const YAML = require('js-yaml')
const fs = require('fs')
const path = require('path')

describe('models: Profile', (context) => {
  context('constructor() - new profile', (assert, next) => {
    var storage = tmp()
    var profile = Profile(storage)
    assert.ok(profile.publicKey, 'has a public key')

    var yaml = YAML.safeLoad(fs.readFileSync(path.join(storage, 'profile.yml')))
    assert.ok(yaml, 'saves as yaml')
    assert.same(yaml.publicKey, profile.publicKey.toString('hex'), 'saves the publicKey')

    cleanup(storage, next)
  })

  context('constructor() - existing profile', (assert, next) => {
    var storage = tmp()
    var name = randomWords(1).pop()
    var identity = crypto.boxKeyPair()

    fs.writeFileSync(path.join(storage, 'profile.yml'), YAML.safeDump({ name, ...identity }))

    var profile = Profile(storage)
    assert.ok(profile.name, 'loads the name')
    assert.ok(profile.publicKey, 'loads the public key')

    cleanup(storage, next)
  })

  context('update(params) - changes name', (assert, next) => {
    var storage = tmp()
    var profile = Profile(storage)
    var name = randomWords(1).pop()

    profile.update({ name })

    assert.same(profile.name, name, 'sets the name')
    cleanup(storage, next)
  })
})
