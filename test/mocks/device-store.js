const duplexify = require('duplexify')
const memdb = require('level-mem')
const LiveStream = require('level-live-stream')

module.exports = function DeviceStore () {
  const db = memdb()
  LiveStream.install(db)

  return {
    source: duplexify(),
    store: db,
    stream: db.createLiveStream()
  }
}
