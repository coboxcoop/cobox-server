const { describe } = require('tape-plus')
const httpMocks = require('node-mocks-http')
const randomWords = require('random-words')

const SystemController = require('../../../app/controllers/system')
const Router = require('../../../app/routes/system')
const system = require('../../../package.json')

const { hex } = require('../../../util')

describe('Routes: /system', (context) => {
  let app, api

  context.beforeEach((c) => {
    api = { system }
    app = { controllers: { system: SystemController(api) } }
  })

  context('GET / - valid with correct parameters', (assert, next) => {
    var request = httpMocks.createRequest({
      method: 'GET',
      url: '/'
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      assert.ok(data, 'gives a response')
      assert.same(data.name, system.name , 'returns the name')
      assert.same(data.description, system.description , 'returns the description')
      assert.same(data.version, system.version , 'returns the version')
      assert.same(data.author, system.author , 'returns the author')
      assert.same(data.license, system.license , 'returns the license')
      assert.same(data.bugs.url, system.bugs.url, 'returns the bugs url')
      next()
    })
  })
})
