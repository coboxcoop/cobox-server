const { describe } = require('tape-plus')
const httpMocks = require('node-mocks-http')
const crypto = require('cobox-crypto')
const Repository = require('@coboxcoop/repository')
const randomWords = require('random-words')
const sinon = require('sinon')
const path = require('path')
const memdb = require('level-mem')
const Invite = require('cobox-key-exchange')
const { assign, values } = Object
const { saveKey } = require('cobox-keys')
const { encodings } = require('cobox-schemas')
const Connection = encodings.peer.connection

const seedResources = require('../../fixtures/resource')
const stubResource = require('../../stubs/resource')
const stubProfile = require('../../stubs/profile')

const SpacesController = require('../../../app/controllers/spaces')
const SpacesValidator = require('../../../app/validators/spaces')
const InvitesValidator = require('../../../app/validators/invites')

const Router = require('../../../app/routes/spaces')

const { hex } = require('../../../util')
const stateFixture = require('../../fixtures/state.json')
const filesFixture = require('../../fixtures/ls.json')

const { tmp, cleanup } = require('../../util')

describe('Router: /spaces', (context) => {
  let stubs, app, api, seeds, storage

  context.beforeEach((c) => {
    storage = tmp()
    stubs = {
      ready: sinon.stub().callsArg(0),
      swarm: sinon.stub(),
      unswarm: sinon.stub(),
      path: storage,
      log: {
        ready: sinon.stub().callsArg(0),
        publish: sinon.stub().resolves()
      },
      drive: {
        // horrific stub of hyperdrive
        // we should be handling the drive in drive-handler
        _drives: {
          [crypto.randomBytes(32)]: {
            metadata: { byteLength: 5 },
            _contentStates: {
              get: sinon.stub().returns({ feed: { byteLength: 100 } })
            }
          }
        },
        ready: sinon.stub().resolves(0),
        ls: sinon.stub().resolves(filesFixture),
        mount: sinon.stub().resolves(true),
        unmount: sinon.stub().resolves(true)
      },
      state: {
        ready: sinon.stub().resolves(),
        query: sinon.stub().returns(stateFixture)
      },
      db: { connections: memdb({ valueEncoding: Connection }) }
    }

    api = {
      spaces: { store: Repository(null, stubResource(storage, stubs)) },
      profile: stubProfile()
    }

    app = {
      controllers: {
        spaces: SpacesController(api)
      },
      validators:  {
        spaces: SpacesValidator(api),
        invites: InvitesValidator(api)
      }
    }

    seeds = seedResources()
    api.spaces.store._seed(seeds)
    entries = values(seeds)
    seed = entries[entries.length - 1]
  })

  context.afterEach((c) => {
    cleanup(storage)
  })

  context('GET / - valid', (assert, next) => {
    const request = httpMocks.createRequest({
      method: 'GET',
      url: '/'
    })

    const response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    const routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      const data = response._getJSONData()
      assert.ok(Array.isArray(data))
      assert.equal(data.length, entries.length)
      next()
    })
  })

  context('GET /:id - valid with correct parameters', (assert, next) => {
    const request = httpMocks.createRequest({
      method: 'GET',
      url: `/${seed.address}`
    })

    const response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    const routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      const data = response._getJSONData()
      assert.notOk(data.errors, 'finds the space')
      assert.ok(data.name, 'returns space name')
      assert.same(data.address, seed.address, 'returns space address')
      next()
    })
  })

  context('GET /:id - invalid if doesnt exist', (assert, next) => {
    const address = hex(crypto.randomBytes(32))

    const request = httpMocks.createRequest({
      method: 'GET',
      url: `/${address}`
    })

    const response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    const routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      const data = response._getJSONData()
      const errors = data.errors
      assert.ok(Array.isArray(errors), 'returns an error')
      assert.same(errors.length, 1, 'returns a single error')
      assert.same(errors[0].msg, 'entry not found', 'returns an error message')
      next()
    })
  })

  context('POST / - valid', (assert, next) => {
    const name = randomWords(1).pop()

    const request = httpMocks.createRequest({
      method: 'POST',
      url: `/`,
      body: { name }
    })

    const response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    const routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      const data = response._getJSONData()
      assert.ok(data, 'creates a space')
      assert.notOk(data.errors, 'has no errors')
      assert.same(data.name, name, 'has the name')
      assert.ok(data.address, 'has an address')
      next()
    })
  })

  context('POST / - valid with an invite code', (assert, next) => {
    const invitedSpace = {
      name: 'magma',
      address: hex(crypto.address()),
      encryptionKey: hex(crypto.randomBytes(32))
    }

    const params = assign({
      publicKey: api.profile.publicKey,
      encryptionKey: crypto.randomBytes(32)
    }, invitedSpace)

    const code = hex(Invite.create(params))

    const request = httpMocks.createRequest({
      method: 'POST',
      url: `/`,
      body: { code }
    })

    const response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    const routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      const data = response._getJSONData()
      assert.ok(data, 'creates the space')
      assert.same(data.name, invitedSpace.name, 'returns the name')
      assert.same(data.address, invitedSpace.address, 'returns the address')
      next()
    })
  })

  context('POST / - invalid if name is a key', (assert, next) => {
    const name = hex(crypto.randomBytes(32))

    const request = httpMocks.createRequest({
      method: 'POST',
      url: `/`,
      body: { name }
    })

    const response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    const routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      const data = response._getJSONData()
      const errors = data.errors[0].nestedErrors
      assert.ok(Array.isArray(errors), 'returns errors')
      assert.same(errors[0].param, 'name', 'name error')
      assert.same(errors[0].msg, 'must be below 32 characters', 'correct message')
      next()
    })
  })

  context(`POST / - invalid if space exists`, (assert, next) => {
    var request = httpMocks.createRequest({
      method: 'POST',
      url: `/`,
      body: { name: seed.name }
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      const errors = data.errors[0].nestedErrors
      assert.ok(Array.isArray(errors), 'returns errors')
      assert.same(errors[0].param, 'name', 'name error')
      assert.same(errors[0].msg, 'already exists', 'correct message')
      next()
    })
  })

  context('POST / - invalid without name as param', (assert, next) => {
    var request = httpMocks.createRequest({
      method: 'POST',
      url: `/`,
      body: { id: 'will-fail' }
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      const data = response._getJSONData()
      const errors = data.errors[0].nestedErrors
      assert.ok(Array.isArray(errors), 'returns errors')
      assert.same(errors[0].param, 'name', 'name error')
      assert.same(errors[0].msg, 'is required', 'correct message')
      next()
    })
  })

  context('GET /:id/connections', (assert, next) => {
    var msgs = [{
      key: hex(crypto.randomBytes(16)),
      value: {
        type: 'peer/connection',
        timestamp: Date.now(),
        content: { connected: true, peer: '' }
      }
    }, {
      key: hex(crypto.randomBytes(16)),
      value: {
        type: 'peer/connection',
        timestamp: Date.now(),
        content: { connected: true, peer: '' }
      }
    }]

    stubs.db.connections.batch(msgs.map((msg) => Object.assign({ type: 'put' }, msg)))

    var request = httpMocks.createRequest({
      method: 'GET',
      url: `/${seed.address}/connections`,
      body: seed
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var connections = response._getJSONData()
      assert.ok(Array.isArray(connections), 'success')
      assert.same(connections.length, 2, 'returns two connections')
      var values = connections.map((msg) => msg.data)
      var seedData = msgs.map((msg) => msg.value)
      assert.same(values, seedData, 'returns connections')
      next()
    })
  })


  context('POST /:id/connections - valid', (assert, next) => {
    var request = httpMocks.createRequest({
      method: 'POST',
      url: `/${seed.address}/connections`,
      body: seed
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      assert.ok(data, 'success')
      assert.notOk(data.errors, 'returns no errors')
      assert.same(data.name, seed.name, 'returns space name')
      assert.same(data.address, seed.address, 'returns space address')
      next()
    })
  })

  context('POST /:id/connections - invalid if doesnt exist', (assert, next) => {
    const address = hex(crypto.randomBytes(32))

    var request = httpMocks.createRequest({
      method: 'POST',
      url: `/${address}/connections`,
      body: { address }
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      let errors = data.errors
      assert.ok(Array.isArray(errors), 'returns an error')
      assert.same(errors.length, 1, 'returns a single error message')
      assert.same(errors[0].msg, 'entry not found', 'returns an error message')
      next()
    })
  })

  context('DELETE /:id/connections - valid', (assert, next) => {
    var request = httpMocks.createRequest({
      method: 'DELETE',
      url: `/${seed.address}/connections`
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      assert.ok(data, 'deletes a connection')
      assert.same(data.name, seed.name, 'returns space name')
      assert.same(data.address, seed.address, 'returns space address')
      next()
    })
  })

  context('DELETE /:id/connections - invalid if doesnt exist', (assert, next) => {
    const address = hex(crypto.randomBytes(32))

    var request = httpMocks.createRequest({
      method: 'DELETE',
      url: `/${address}/connections`
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      let errors = data.errors
      assert.ok(Array.isArray(errors), 'returns an error')
      assert.same(errors.length, 1, 'returns a single error message')
      assert.same(errors[0].msg, 'entry not found', 'returns an error message')
      next()
    })
  })

  context('GET /:id/drive/history - valid', (assert, next) => {
    // const defaultQuery = { $filter: { value: { timestamp: { $gt: 0 } } } }

    const request = httpMocks.createRequest({
      method: 'GET',
      url: `/${seed.address}/drive/history`
    })

    const routeHandler = Router(app)
    const response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    routeHandler(request, response)

    response.on('end', () => {
      // assert.ok(seed.drive.query.calledWith(defaultQuery), 'default state query set')
      const data = response._getJSONData()
      assert.ok(Array.isArray(data), 'returns an array')
      assert.same(data, stateFixture, 'returns current drive state')
      next()
    })
  })

  context('GET /:id/drive/readdir - valid', (assert, next) => {
    var request = httpMocks.createRequest({
      method: 'GET',
      url: `/${seed.address}/drive/readdir`
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    const routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      const data = response._getJSONData()
      assert.same(data, filesFixture, 'returns list of files')
      next()
    })
  })

  context('GET /:id/drive/stat - valid', (assert, next) => {
    var request = httpMocks.createRequest({
      method: 'GET',
      url: `/${seed.address}/drive/stat`
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    const routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      const data = response._getJSONData()
      assert.same(data, { size: 105 }, 'returns size of all drives')
      next()
    })
  })

  context('POST /:id/mounts - valid', (assert, next) => {
    const location = '/cobox'

    const request = httpMocks.createRequest({
      method: 'POST',
      url: `/${seed.address}/mounts`,
      body: { location }
    })

    const response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    const routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      const data = response._getJSONData()
      assert.ok(data, 'creates a space')
      assert.notOk(data.errors, 'returns no errors')
      assert.same(data.name, seed.name, 'returns space name')
      assert.same(data.address, seed.address, 'returns space address')
      assert.same(data.location, path.join(location, seed.name), 'returns spaces mount location')
      next()
    })
  })

  context('POST /:id/mounts - invalid', (assert, next) => {
    const location = '/cobox'
    const id = hex(crypto.randomBytes(32))

    const request = httpMocks.createRequest({
      method: 'POST',
      url: `/${id}/mounts`,
      body: { location }
    })

    const response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    const routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      const data = response._getJSONData()
      const errors = data.errors
      assert.ok(Array.isArray(errors), 'returns an error')
      assert.same(errors.length, 1, 'returns a single error')
      assert.same(errors[0].msg, 'entry not found', 'returns an error message')
      next()
    })
  })

  context('DELETE /:id/mounts - valid', (assert, next) => {
    const request = httpMocks.createRequest({
      method: 'DELETE',
      url: `/${seed.address}/mounts`
    })

    const response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    const routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      const data = response._getJSONData()
      assert.ok(data, 'unmounts the space')
      assert.notOk(data.errors, 'returns no errors')
      assert.same(data.name, seed.name, 'returns the name')
      assert.same(data.address, seed.address, 'returns the address')
      next()
    })
  })

  context('DELETE /:id/mounts - invalid', (assert, next) => {
    const id = 'not-valid'

    request = httpMocks.createRequest({
      method: 'DELETE',
      url: `/${id}/mounts`,
    })

    const response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    const routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      const data = response._getJSONData()
      let errors = data.errors
      assert.ok(Array.isArray(errors), 'returns an error')
      assert.same(errors.length, 1, 'returns a single error')
      assert.same(errors[0].msg, 'entry not found', 'returns an error message')
      next()
    })
  })

  context('GET /invites/accept - valid', (assert, next) => {
    const invitedSpace = {
      name: 'magma',
      address: hex(crypto.address())
    }

    const params = assign({
      publicKey: hex(api.profile.publicKey),
      encryptionKey: hex(crypto.randomBytes(32))
    }, invitedSpace)

    const code = hex(Invite.create(params))

    request = httpMocks.createRequest({
      method: 'GET',
      url: `/invites/accept`,
      query: { code }
    })

    const response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    const routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      const data = response._getJSONData()
      assert.same(data.name, invitedSpace.name, 'returns the space name')
      assert.same(data.address, invitedSpace.address, 'returns the space address')
      assert.ok(stubs.swarm.calledOnce, 'swarms on the space')
      next()
    })
  })

  context('POST /:id/invites - valid', (assert, next) => {
    saveKey(storage, 'encryption_key', crypto.randomBytes(32))
    const publicKey = hex(crypto.boxKeyPair().publicKey)

    request = httpMocks.createRequest({
      method: 'POST',
      url: `/${seed.address}/invites`,
      body: { publicKey }
    })

    const response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    const routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      const data = response._getJSONData()
      assert.same(data.type, 'peer/invite', 'returns an invite')
      assert.same(data.content.publicKey, publicKey, 'returns the public key')
      assert.ok(data.content.code, 'returns an invite code')
      next()
    })
  })
})
