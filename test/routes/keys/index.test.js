const { describe } = require('tape-plus')
const httpMocks = require('node-mocks-http')
const crypto = require('cobox-crypto')
const { saveKey } = require('cobox-keys')
const Repository = require('@coboxcoop/repository')
const randomWords = require('random-words')
const path = require('path')
const os = require('os')
const sinon = require('sinon')

const seedResources = require('../../fixtures/resource')
const stubResource = require('../../stubs/resource')

const Router = require('../../../app/routes/keys')

const KeysController = require('../../../app/controllers/keys')
const SpacesController = require('../../../app/controllers/spaces')
const KeysValidator = require('../../../app/validators/keys')

const { cleanup, tmp } = require('../../util')

describe('Routes: /keys', (context) => {
  let app, api, seeds
  let storage, dir

  context.beforeEach((c) => {
    storage = tmp()
    dir = tmp()
    stubs = { path: storage, ready: sinon.stub().callsArg(0) }

    api = {
      spaces: { store: Repository(null, stubResource(storage, stubs)) },
      settings: { storage }
    }

    app = {
      controllers: {
        spaces: SpacesController(api),
        keys: KeysController(api)
      },
      validators: {
        keys: KeysValidator(api),
      }
    }

    seeds = seedResources()
    api.spaces.store._seed(seeds)

    saveKey(storage, 'parent_key', crypto.randomBytes(32))
    saveKey(storage, 'encryption_key', crypto.randomBytes(32))
  })

  context('PUT /export - responds with the file path', (assert, next) => {
    var request = httpMocks.createRequest({
      method: 'PUT',
      url: '/export',
      body: { dir }
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      assert.ok(data, 'gets a response')
      assert.notOk(data.errors, 'returns no errors')
      assert.ok(data.filename, 'returns a filename')
      assert.equal(path.basename(data.filename), 'key-backup.pdf', 'returns the path')
      cleanup([storage, dir], next)
    })
  })

  context('PUT /export - defaults to os.homedir()', (assert, next) => {
    var request = httpMocks.createRequest({
      method: 'PUT',
      url: '/export'
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      assert.ok(data, 'gets a response')
      assert.notOk(data.errors, 'returns no errors')
      assert.ok(data.filename, 'returns a filename')
      var filepath = path.join(os.homedir(), 'key-backup.pdf')
      assert.equal(filepath, data.filename, 'returns the path')
      cleanup([storage, filepath], next)
    })
  })

  context('PUT /export - fails if dir is invalid', (assert, next) => {
    var request = httpMocks.createRequest({
      method: 'PUT',
      url: '/export',
      body: { dir: 231 },
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      let errors = data.errors
      assert.ok(Array.isArray(errors), 'has errors')
      assert.same(errors.length, 1, 'returns a single error')
      assert.same(errors[0].msg, '\'dir\' must be a string', 'returns an error message')
      cleanup(storage, next)
    })
  })
})
