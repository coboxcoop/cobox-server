const { describe } = require('tape-plus')
const httpMocks = require('node-mocks-http')
const crypto = require('cobox-crypto')
const randomWords = require('random-words')

const stubProfile = require('../../stubs/profile')
const ProfileController = require('../../../app/controllers/profile')
const ProfileValidator = require('../../../app/validators/profile')
const Router = require('../../../app/routes/profile')

const { hex } = require('../../../util')

describe('Routes: /profile', (context) => {
  let app, api

  context.beforeEach((c) => {
    api = { profile: stubProfile() }

    app = {
      controllers: { profile: ProfileController(api) },
      validators:  { profile: ProfileValidator(api) }
    }
  })

  context('GET / - valid with correct parameters', (assert, next) => {
    var request = httpMocks.createRequest({
      method: 'GET',
      url: '/'
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      assert.ok(data, 'gives a response')
      assert.same(data.name, api.profile.name, 'returns a name')
      assert.same(data.publicKey, hex(api.profile.publicKey), 'returns a public key')
      next()
    })
  })

  context('PATCH /profile - valid with correct parameters', (assert, next) => {
    const params = { name: randomWords(1).pop() }

    var request = httpMocks.createRequest({
      method: 'PATCH',
      url: '/',
      body: params
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      assert.ok(data, 'gives a response')
      assert.ok(api.profile.update.calledWith(params), 'calls update()')
      assert.ok(data.name, 'returns a name')
      assert.same(data.publicKey, hex(api.profile.publicKey), 'returns a public key')
      next()
    })
  })
})
