const { describe } = require('tape-plus')
const httpMocks = require('node-mocks-http')
const crypto = require('cobox-crypto')
const Repository = require('@coboxcoop/repository')
const randomWords = require('random-words')
const sinon = require('sinon')
const memdb = require('level-mem')
const { encodings } = require('cobox-schemas')
const Connection = encodings.peer.connection
const { values } = Object

const seedResources = require('../../fixtures/resource')
const stubResource = require('../../stubs/resource')
const stubProfile = require('../../stubs/profile')

const ReplicatorsController = require('../../../app/controllers/replicators')
const ReplicatorsValidator = require('../../../app/validators/replicators')

const Router = require('../../../app/routes/replicators')

const { hex } = require('../../../util')

describe('Routes: /replicators', (context) => {
  context.beforeEach((c) => {
    stubs = {
      ready: sinon.stub().callsArg(0),
      swarm: sinon.stub(),
      unswarm: sinon.stub(),
      db: { connections: memdb({ valueEncoding: Connection }) }
    }

    api = {
      replicators: { store: Repository(null, stubResource(storage, stubs)) },
      profile: stubProfile()
    }

    app = {
      controllers: { replicators: ReplicatorsController(api) },
      validators:  { replicators: ReplicatorsValidator(api) }
    }

    seeds = seedResources()
    api.replicators.store._seed(seeds)
    entries = values(seeds)
    seed = entries[entries.length - 1]
  })

  context('GET / - valid', (assert, next) => {
    const request = httpMocks.createRequest({
      method: 'GET',
      url: '/'
    })

    const response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    const routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      const data = response._getJSONData()
      assert.ok(Array.isArray(data), 'returns an array')
      assert.equal(data.length, entries.length, 'response correct')
      next()
    })
  })

  context('POST / - valid', (assert, next) => {
    const name = 'magma'
    const address = hex(crypto.address())

    const request = httpMocks.createRequest({
      method: 'POST',
      url: `/`,
      body: {
        address,
        name
      }
    })

    const response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    const routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      const replicator = response._getJSONData()
      assert.ok(replicator, 'creates a replicator')
      assert.same(replicator.name, name, 'matches the name')
      assert.ok(replicator.address, 'has an address')
      next()
    })
  })

  context('POST / - invalid if name is a key', (assert, next) => {
    const name = hex(crypto.randomBytes(32))
    const address = hex(crypto.address())

    const request = httpMocks.createRequest({
      method: 'POST',
      url: `/`,
      body: {
        address,
        name
      }
    })

    const response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    const routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      const data = response._getJSONData()
      let errors = data.errors
      assert.ok(Array.isArray(errors), 'returns an error')
      assert.same(errors.length, 1, 'returns a single error')
      assert.same(errors[0].param, 'name', 'name errors')
      assert.same(errors[0].msg, 'must be below 32 characters', 'correct message')
      next()
    })
  })

  context('POST / - invalid if replicator with name exists', (assert, next) => {
    const address = hex(crypto.address())

    const request = httpMocks.createRequest({
      method: 'POST',
      url: `/`,
      body: {
        address,
        name: seed.name
      }
    })

    const response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    const routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      const data = response._getJSONData()
      let errors = data.errors
      assert.ok(Array.isArray(errors), 'returns an error')
      assert.same(errors.length, 1, 'returns a single error')
      assert.same(errors[0].param, 'name', 'name error')
      assert.same(errors[0].msg, 'already exists', 'correct message')
      next()
    })
  })

  context('POST / - invalid if replicator with address exists', (assert, next) => {
    const name = randomWords(1).pop()

    const request = httpMocks.createRequest({
      method: 'POST',
      url: `/`,
      body: {
        address: seed.address,
        name: name
      }
    })

    const response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    const routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      const data = response._getJSONData()
      let errors = data.errors
      assert.ok(Array.isArray(errors), 'returns an error')
      assert.same(errors.length, 1, 'returns a single error')
      assert.same(errors[0].param, 'address', 'address error')
      assert.same(errors[0].msg, `already exists`, 'correct message')
      next()
    })
  })

  context('POST / - blank params', (assert, next) => {
    const request = httpMocks.createRequest({
      method: 'POST',
      url: `/`
    })

    const response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    const routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      const data = response._getJSONData()
      let errors = data.errors
      assert.ok(Array.isArray(errors), 'returns an error')
      assert.same(errors.length, 2, 'returns two errors')
      assert.same(errors, [{
        param: 'name',
        location: 'body',
        msg: 'is required'
      }, {
        param: 'address',
        location: 'body',
        msg: 'is required'
      }], 'correct errors')
      next()
    })
  })

  context('POST /:id/connections - valid', (assert, next) => {
    var request = httpMocks.createRequest({
      method: 'POST',
      url: `/${seed.address}/connections`,
      body: seed
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var replicator = response._getJSONData()
      assert.ok(replicator, 'success')
      assert.notOk(replicator.errors, 'returns no errors')
      assert.same(replicator.name, seed.name, 'returns replicator name')
      assert.same(replicator.address, seed.address, 'returns replicator address')
      next()
    })
  })

  context('POST /:id/connections - invalid if doesnt exist', (assert, next) => {
    const address = hex(crypto.randomBytes(32))

    var request = httpMocks.createRequest({
      method: 'POST',
      url: `/${address}/connections`,
      body: { address }
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      let errors = data.errors
      assert.ok(Array.isArray(errors), 'returns an error')
      assert.same(errors.length, 1, 'returns a single error message')
      assert.same(errors[0].msg, 'entry not found', 'returns an error message')
      next()
    })
  })

  context('DELETE /:id/connections - valid', (assert, next) => {
    var request = httpMocks.createRequest({
      method: 'DELETE',
      url: `/${seed.address}/connections`
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var replicator = response._getJSONData()
      assert.ok(replicator, 'deletes a connection')
      assert.same(replicator.name, seed.name, 'returns replicator name')
      assert.same(replicator.address, seed.address, 'returns replicator address')
      next()
    })
  })

  context('DELETE /:id/connections - invalid if doesnt exist', (assert, next) => {
    const address = hex(crypto.randomBytes(32))

    var request = httpMocks.createRequest({
      method: 'DELETE',
      url: `/${address}/connections`
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      let errors = data.errors
      assert.ok(Array.isArray(errors), 'returns an error')
      assert.same(errors.length, 1, 'returns a single error message')
      assert.same(errors[0].msg, 'entry not found', 'returns an error message')
      next()
    })
  })
})
