const { describe } = require('tape-plus')
const { saveKey } = require('cobox-keys')
const httpMocks = require('node-mocks-http')
const crypto = require('cobox-crypto')
const Repository = require('@coboxcoop/repository')
const randomWords = require('random-words')
const memdb = require('level-mem')
const sinon = require('sinon')
const Invite = require('cobox-key-exchange')
const { assign, values } = Object
const { encodings } = require('cobox-schemas')
const Connection = encodings.peer.connection

const seedResources = require('../../fixtures/resource')
const stubResource = require('../../stubs/resource')
const stubProfile = require('../../stubs/profile')

const AdminController = require('../../../app/controllers/admin')

const AdminValidator = require('../../../app/validators/admin')
const InvitesValidator = require('../../../app/validators/invites')

const Router = require('../../../app/routes/admin')

const { hex } = require('../../../util')

const { tmp, cleanup } = require('../../util')

describe('Routes: /admin', (context) => {
  let app, api, seeds, seed, entries

  context.beforeEach((c) => {
    storage = tmp()
    stubs = {
      ready: sinon.stub().callsArg(0),
      swarm: sinon.stub().resolves(true),
      unswarm: sinon.stub().resolves(true),
      path: storage,
      log: { publish: sinon.stub().resolves(true) },
      db: { connections: memdb({ valueEncoding: Connection }) }
    }

    identity = { name: randomWords(1).pop(), publicKey: crypto.randomBytes(32) }

    api = {
      profile: stubProfile(),
      devices: { store: sinon.stub() },
      admin: {
        devices: {
          store: Repository(null, stubResource(storage, stubs))
        }
      }
    }

    app = {
      controllers: {
        admin: AdminController(api),
      },
      validators: {
        admin: AdminValidator(api),
        invites: InvitesValidator(api)
      }
    }

    seeds = seedResources()

    api.admin.devices.store._seed(seeds)
    entries = values(seeds)
    seed = entries[entries.length - 1]
  })

  context.afterEach((c) => {
    cleanup(storage)
  })

  context('GET /devices - valid with correct parameters', (assert, next) => {
    const request = httpMocks.createRequest({
      method: 'GET',
      url: '/devices',
    })

    const response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    const routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      const data = response._getJSONData()
      assert.ok(Array.isArray(data))
      assert.equal(data.length, entries.length)
      next()
    })
  })

  context('GET /devices/:id - valid with correct parameters', (assert, next) => {
    var request = httpMocks.createRequest({
      method: 'GET',
      url: `/devices/${seed.address}`
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var device = response._getJSONData()
      assert.notOk(device.errors, 'finds the device')
      assert.ok(device.name, 'returns device name')
      assert.same(device.address, seed.address, 'returns device address')
      next()
    })
  })

  context(`GET /devices/:id - invalid if doesn't exist`, (assert, next) => {
    const address = hex(crypto.randomBytes(32))

    var request = httpMocks.createRequest({
      method: 'GET',
      url: `/devices/${address}`
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      let errors = data.errors
      assert.ok(Array.isArray(errors), 'returns an error')
      assert.same(errors.length, 1, 'returns a single error')
      assert.same(errors[0].msg, 'entry not found', 'returns an error message')
      next()
    })
  })

  context('POST /devices - valid', (assert, next) => {
    var name = 'magma'
    var address = hex(crypto.randomBytes(32))
    var encryptionKey = hex(crypto.randomBytes(32))

    var request = httpMocks.createRequest({
      method: 'POST',
      url: `/devices`,
      body: {
        name,
        address,
        encryptionKey
      }
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var device = response._getJSONData()
      assert.ok(stubs.log.publish.calledTwice, 'publishes peer/about and space/about')
      assert.ok(device, 'creates a device')
      assert.notOk(device.errors, 'has no errors')
      assert.same(device.name, name, 'has the name')
      assert.same(device.address, address, 'has the address')
      next()
    })
  })

  context('POST /devices - with commands ', (assert, next) => {
    var name = 'magma'
    var address = hex(crypto.randomBytes(32))
    var encryptionKey = hex(crypto.randomBytes(32))

    var request = httpMocks.createRequest({
      method: 'POST',
      url: `/devices`,
      body: {
        name,
        address,
        encryptionKey,
        commands: [{ action: 'hide' }]
      }
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var device = response._getJSONData()
      assert.ok(stubs.log.publish.calledThrice, 'publishes command/hide in commands controller')
      assert.ok(device, 'creates a device')
      assert.notOk(device.errors, 'has no errors')
      assert.same(device.name, name, 'has the name')
      assert.same(device.address, address, 'has the address')
      next()
    })
  })

  context('POST /devices - invalid if name is a key', (assert, next) => {
    var name = hex(crypto.randomBytes(32))
    var address = hex(crypto.randomBytes(32))
    var encryptionKey = hex(crypto.randomBytes(32))

    var request = httpMocks.createRequest({
      method: 'POST',
      url: `/devices`,
      body: {
        name,
        address,
        encryptionKey
      }
    })

    var routeHandler = Router(app)
    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      let errors = data.errors
      assert.ok(Array.isArray(errors), 'returns an error')
      assert.same(errors.length, 1, 'returns a single error')
      assert.same(errors[0].param, 'name', 'name error')
      assert.same(errors[0].msg, 'must be below 32 characters', 'correct message')
      next()
    })
  })

  context('POST /devices - invalid if device exists', (assert, next) => {
    var encryptionKey = hex(crypto.randomBytes(32))

    var request = httpMocks.createRequest({
      method: 'POST',
      url: `/devices`,
      body: assign(seed, { encryptionKey })
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      let errors = data.errors
      assert.ok(Array.isArray(errors), 'returns errors')
      assert.same(errors[0].param,'name', 'name error')
      assert.same(errors[0].msg, `already exists`, 'correct message')
      next()
    })
  })

  context('POST /devices - invalid without name as param', (assert, next) => {
    var address = hex(crypto.randomBytes(32))
    var encryptionKey = hex(crypto.randomBytes(32))

    var request = httpMocks.createRequest({
      method: 'POST',
      url: `/devices`,
      body: {
        address,
        encryptionKey
      }
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      let errors = data.errors
      assert.ok(Array.isArray(errors), 'returns an error')
      assert.same(errors.length, 1, 'returns a single error')
      assert.same(errors[0].param, 'name', 'name error')
      assert.same(errors[0].msg, 'is required', 'correct message')
      next()
    })
  })

  context('POST /devices - invalid address', (assert, next) => {
    var name = 'magma'
    var address = 'will-fail'
    var encryptionKey = hex(crypto.randomBytes(32))

    var request = httpMocks.createRequest({
      method: 'POST',
      url: `/devices`,
      body: {
        name,
        address,
        encryptionKey
      }
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      let errors = data.errors[0].nestedErrors
      assert.ok(Array.isArray(errors), 'returns an error')
      assert.same(errors.length, 2, 'returns a two errors')
      assert.same(errors[0].param, 'address', 'address error')
      assert.same(errors[0].msg, 'must be a 32 byte key', 'correct message')
      next()
    })
  })

  context('POST /devices - invalid encryptionKey', (assert, next) => {
    var name = 'magma'
    var address = hex(crypto.randomBytes(32))
    var encryptionKey = 'will-fail'

    var request = httpMocks.createRequest({
      method: 'POST',
      url: `/devices`,
      body: {
        name,
        address,
        encryptionKey
      }
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      let errors = data.errors[0].nestedErrors
      assert.ok(Array.isArray(errors), 'returns errors')
      assert.same(errors.length, 2, 'returns a two errors')
      assert.same(errors[0].param, 'encryptionKey', 'encryptionKey errors')
      assert.same(errors[0].msg, 'must be a 32 byte key', 'correct message')
      next()
    })
  })

  context('GET /devices/:id/connections', (assert, next) => {
    var msgs = [{
      key: hex(crypto.randomBytes(16)),
      value: {
        type: 'peer/connection',
        timestamp: Date.now(),
        content: { connected: true, peer: '' }
      }
    }, {
      key: hex(crypto.randomBytes(16)),
      value: {
        type: 'peer/connection',
        timestamp: Date.now(),
        content: { connected: true, peer: '' }
      }
    }]

    stubs.db.connections.batch(msgs.map((msg) => Object.assign({ type: 'put' }, msg)))

    var request = httpMocks.createRequest({
      method: 'GET',
      url: `/devices/${seed.address}/connections`,
      body: seed
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var connections = response._getJSONData()
      assert.ok(Array.isArray(connections), 'success')
      assert.same(connections.length, 2, 'returns two connections')
      var values = connections.map((msg) => msg.data)
      var seedData = msgs.map((msg) => msg.value)
      assert.same(values, seedData, 'returns connections')
      next()
    })
  })

  context('POST /devices/:id/connections - valid', (assert, next) => {
    var request = httpMocks.createRequest({
      method: 'POST',
      url: `/devices/${seed.address}/connections`,
      body: seed
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var device = response._getJSONData()
      assert.ok(device, 'success')
      assert.notOk(device.errors, 'returns no errors')
      assert.same(device.name, seed.name, 'returns device name')
      assert.same(device.address, seed.address, 'returns device address')
      next()
    })
  })

  context('POST /devices/:id/connections - invalid if doesnt exist', (assert, next) => {
    const address = hex(crypto.randomBytes(32))

    var request = httpMocks.createRequest({
      method: 'POST',
      url: `/devices/${address}/connections`,
      body: { address }
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      let errors = data.errors
      assert.ok(Array.isArray(errors), 'returns an error')
      assert.same(errors.length, 1, 'returns a single error message')
      assert.same(errors[0].msg, 'entry not found', 'returns an error message')
      next()
    })
  })

  context('DELETE /devices/:id/connections - valid', (assert, next) => {
    var request = httpMocks.createRequest({
      method: 'DELETE',
      url: `/devices/${seed.address}/connections`
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var device = response._getJSONData()
      assert.ok(device, 'deletes a connection')
      assert.same(device.name, seed.name, 'returns device name')
      assert.same(device.address, seed.address, 'returns device address')
      next()
    })
  })

  context('DELETE /devices/:id/connections - invalid if doesnt exist', (assert, next) => {
    const address = hex(crypto.randomBytes(32))

    var request = httpMocks.createRequest({
      method: 'DELETE',
      url: `/devices/${address}/connections`
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      let errors = data.errors
      assert.ok(Array.isArray(errors), 'returns an error')
      assert.same(errors.length, 1, 'returns a single error message')
      assert.same(errors[0].msg, 'entry not found', 'returns an error message')
      next()
    })
  })

  context('POST /devices/:id/commands/announce', (assert, next) => {
    var request = httpMocks.createRequest({
      method: 'POST',
      url: `/devices/${seed.address}/commands/announce`
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      assert.ok(data, 'success')
      assert.notOk(data.errors, 'returns no errors')
      assert.same(data.type, 'command/announce', 'returns the command')
      assert.same(data.author, hex(api.profile.publicKey), 'signed by correct author')
      next()
    })
  })

  context('POST /devices/:id/commands/announce - invalid if doesnt exist', (assert, next) => {
    const address = hex(crypto.randomBytes(32))

    var request = httpMocks.createRequest({
      method: 'POST',
      url: `/devices/${address}/commands/announce`
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      let errors = data.errors
      assert.ok(Array.isArray(errors), 'returns an error')
      assert.same(errors.length, 1, 'returns a single error message')
      assert.same(errors[0].msg, 'entry not found', 'returns an error message')
      next()
    })
  })

  context('POST /devices/:id/commands/hide', (assert, next) => {
    var request = httpMocks.createRequest({
      method: 'POST',
      url: `/devices/${seed.address}/commands/hide`
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      assert.ok(data, 'success')
      assert.notOk(data.errors, 'returns no errors')
      assert.same(data.type, 'command/hide', 'returns the command')
      assert.same(data.author, hex(api.profile.publicKey), 'signed by correct author')
      next()
    })
  })

  context('POST /devices/:id/commands/hide - invalid if doesnt exist', (assert, next) => {
    const address = hex(crypto.randomBytes(32))

    var request = httpMocks.createRequest({
      method: 'POST',
      url: `/devices/${address}/commands/hide`
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      let errors = data.errors
      assert.ok(Array.isArray(errors), 'returns an error')
      assert.same(errors.length, 1, 'returns a single error message')
      assert.same(errors[0].msg, 'entry not found', 'returns an error message')
      next()
    })
  })

  context('POST /devices/:id/commands/replicate', (assert, next) => {
    var address = hex(crypto.randomBytes(32))
    var name = randomWords(1).pop()

    var request = httpMocks.createRequest({
      method: 'POST',
      url: `/devices/${seed.address}/commands/replicate`,
      body: { name, address }
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      assert.ok(data, 'success')
      assert.notOk(data.errors, 'returns no errors')
      assert.same(data.type, 'command/replicate', 'returns the command')
      assert.same(data.author, hex(api.profile.publicKey), 'signed by correct author')
      assert.same(data.content.address, address, 'returns the address')
      next()
    })
  })

  context('POST /devices/:id/commands/unreplicate', (assert, next) => {
    var address = hex(crypto.randomBytes(32))
    var name = randomWords(1).pop()

    var request = httpMocks.createRequest({
      method: 'POST',
      url: `/devices/${seed.address}/commands/unreplicate`,
      body: { address, name }
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      assert.ok(data, 'success')
      assert.notOk(data.errors, 'returns no errors')
      assert.same(data.type, 'command/unreplicate', 'returns the command')
      assert.same(data.author, hex(api.profile.publicKey), 'signed by correct author')
      assert.same(data.content.address, address, 'returns the address')
      next()
    })
  })

  context('GET /devices/invites/accept - valid', (assert, next) => {
    const invitedDevice = {
      name: 'magma',
      address: hex(crypto.address())
    }

    const params = assign({
      publicKey: api.profile.publicKey,
      encryptionKey: crypto.randomBytes(32)
    }, invitedDevice)

    const code = hex(Invite.create(params))

    request = httpMocks.createRequest({
      method: 'GET',
      url: `/devices/invites/accept`,
      query: { code }
    })

    const response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    const routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      const data = response._getJSONData()
      assert.same(data.name, invitedDevice.name, 'returns the device name')
      assert.same(data.address, invitedDevice.address, 'returns the device address')
      assert.ok(stubs.swarm.calledOnce, 'swarms on the device')
      next()
    })
  })

  context('POST /devices/:id/invites - valid', (assert, next) => {
    saveKey(storage, 'encryption_key', crypto.randomBytes(32))
    const publicKey = hex(crypto.boxKeyPair().publicKey)

    request = httpMocks.createRequest({
      method: 'POST',
      url: `/devices/${seed.address}/invites`,
      body: { publicKey }
    })

    const response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    const routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      const data = response._getJSONData()
      assert.same(data.type, 'peer/invite', 'returns an invite')
      assert.same(data.content.publicKey, publicKey, 'returns the public key')
      assert.ok(data.content.code, 'returns an invite code')
      next()
    })
  })
})
