const { describe } = require('tape-plus')
const sinon = require('sinon')
const crypto = require('cobox-crypto')
const Repository = require('@coboxcoop/repository')
const randomWords = require('random-words')
const omit = require('lodash.omit')
const { values } = Object

const AdminDevicesController = require('../../../../app/controllers/admin/devices')
const seedResources = require('../../../fixtures/resource')
const stubResource = require('../../../stubs/resource')
const stubProfile = require('../../../stubs/profile')

const { hex } = require('../../../../util')

describe('Controllers: admin/devices', (context) => {
  let api, stubs, seeds, controller

  context.beforeEach((c) => {
    stubs = {
      ready: sinon.stub().callsArg(0),
      log: { publish: sinon.stub().resolves() },
      swarm: sinon.stub()
    }

    api = {
      admin: { devices: { store: Repository(null, stubResource(null, stubs)) } },
      devices: { store: sinon.stub() },
      profile: stubProfile()
    }

    seeds = seedResources()
    api.admin.devices.store._seed(seeds)
    api.admin.devices.store.isReady = true
    entries = values(seeds)
    controller = AdminDevicesController(api)
  })

  context('index: lists all admin devices', async (assert, next) => {
    const devices = await controller.index(null, { decorate: true })
    assert.same(devices, entries, 'gets all devices')
    next()
  })

  context('create: creates a new device', async (assert, next) => {
    const params = {
      name: randomWords(1).pop(),
      address: hex(crypto.address()),
      encryptionKey: hex(crypto.randomBytes(32))
    }

    const device = await controller.create(params, { decorate: true })
    const check = omit(params, ['encryptionKey'])
    assert.same(device, check, 'creates a device')
    next()
  })

  // TODO
  context('create:  from an invite code', async (assert, next) => {
    next()
  })

  context('create: processes nested commands', async (assert, next) => {
    const commands = [
      { action: 'announce' },
      { action: 'invalid-command' },
      { action: 'replicate', name: 'magma', address: hex(crypto.address()) }
    ]

    const params = {
      name: randomWords(1).pop(),
      address: hex(crypto.address()),
      encryptionKey: hex(crypto.randomBytes(32)),
      commands
    }

    const device = await controller.create(params, { decorate: true })
    assert.same(stubs.log.publish.callCount, 4, 'publishes 4 messages')
    next()
  })

  context('show: gets a device', async (assert, next) => {
    const seed = values(seeds)[values(seeds).length - 1]
    const device = await controller.show(seed, { decorate: true })
    assert.same(device, seed, 'shows a device')
    next()
  })
})
