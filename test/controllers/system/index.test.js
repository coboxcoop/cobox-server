const { describe } = require('tape-plus')
const randomWords = require('random-words')
const SystemController = require('../../../app/controllers/system')
const { hex } = require('../../../util')
const sinon = require('sinon')
const crypto = require('cobox-crypto')
const system = require('../../../package.json')

describe('Controllers: system', (context) => {
  let api, controller

  context.beforeEach((c) => {
    api = { system }
    controller = SystemController(api)
  })

  context('show: gets system information', async (assert, next) => {
    const data = await controller.show()
    assert.ok(data, 'returns your profile')
    assert.ok(data.name, 'returns the name')
    assert.ok(data.description, 'returns the description')
    assert.ok(data.version, 'returns the version')
    assert.ok(data.author, 'returns the author')
    assert.ok(data.license, 'returns the license')
    assert.ok(data.bugs.url, 'returns the bugs url')
    next()
  })
})
