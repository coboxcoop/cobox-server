const { describe } = require('tape-plus')
const sinon = require('sinon')
const crypto = require('cobox-crypto')
const Repository = require('@coboxcoop/repository')
const randomWords = require('random-words')
const { values } = Object

const seedResources = require('../../fixtures/resource')
const stubResource = require('../../stubs/resource')
const ReplicatorsController = require('../../../app/controllers/replicators')

const { hex } = require('../../../util')

describe('Controllers: replicators', (context) => {
  let api, stubs, seeds, controller

  context.beforeEach((c) => {
    stubs = { ready: sinon.stub().callsArg(0) }
    api = { replicators: { store: Repository(null, stubResource(null, stubs)) } }
    seeds = seedResources()
    api.replicators.store._seed(seeds)
    entries = values(seeds)
    controller = ReplicatorsController(api)
  })

  context('index: lists all replicators', async (assert, next) => {
    const replicators = await controller.index(null, { decorate: true })
    assert.same(replicators, entries, 'success')
    next()
  })

  context('create: creates a replicator', async (assert, next) => {
    const params = {
      name: randomWords(1).pop(),
      address: hex(crypto.address())
    }

    const replicator = await controller.create(params, { decorate: true })
    assert.same(replicator, params, 'success')
    next()
  })
  context('show: finds a replicator', async (assert, next) => {
    const seed = entries[entries.length - 1]
    const replicator = await controller.show(seed, { decorate: true })
    assert.same(replicator, seed, 'success')
    next()
  })
})
