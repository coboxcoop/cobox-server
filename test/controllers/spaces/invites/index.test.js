const { describe } = require('tape-plus')
const sinon = require('sinon')
const crypto = require('cobox-crypto')
const Repository = require('@coboxcoop/repository')
const randomWords = require('random-words')
const proxyquire = require('proxyquire')
const Inviter = require('cobox-key-exchange')
const { assign, values  } = Object
const omit = require('lodash.omit')

const seedResources = require('../../../fixtures/resource')
const stubResource = require('../../../stubs/resource')
const stubProfile = require('../../../stubs/profile')

const { hex } = require('../../../../util')

const code = `
dc9ec850387bd699ace4d2d6c00f9a36f
63fb19bf917c3797cb5595ece38e3bc81
3ef2c43e21529641d21e14d05ab71ae90
aaab925a853592e5f456a8a8034b9615c
b79d70a19dd05e08da1a03457c17e9963
eb1793e28a9c89bf65d229abb4d4aae0e
9270d3921d1645e1f68763aa3ece1f880
4c461df6829d23f465f23de457b857280
ce298c174a08c65d8d
`.replace(/\r?\n|\r/g, '')

describe('Controllers: spaces/invites', (context) => {
  let api, stubs, seeds, controller, seed
  let publicKey

  context.beforeEach((c) => {
    profile = stubProfile()
    publicKey = hex(crypto.boxKeyPair().publicKey)

    invite = {
      type: 'peer/invite',
      author: hex(profile.publicKey),
      content: { code, publicKey }
    }

    stubs = {
      ready: sinon.stub().callsArg(0),
      swarm: sinon.stub().returns(true),
      encryptionKey: hex(crypto.randomBytes(32)),
      log: { publish: sinon.stub().resolves(invite) }
    }

    api = {
      spaces: { store: Repository(null, stubResource(null, stubs)) },
      profile
    }

    seeds = seedResources()
    api.spaces.store._seed(seeds)
    seed = values(seeds)[values(seeds).length - 1]

    spaceParams = {
      encryptionKey: crypto.randomBytes(32),
      address: crypto.randomBytes(32),
      name: 'magma'
    }

    inviter = {
      open: sinon.stub().returns(spaceParams),
      create: sinon.stub().returns(code)
    }

    ProxyInvitesController = proxyquire('../../../../app/controllers/spaces/invites', {
      'cobox-key-exchange': inviter
    })

    spacesStub = {
      show: sinon.stub().resolves(api.spaces.store.findBy(seed)),
      create: sinon.stub().resolves(api.spaces.store.create(spaceParams)),
      connections: { create: sinon.stub().resolves(true) }
    }
    controller = ProxyInvitesController(api, spacesStub)
  })

  context('create: create an invite', async (assert, next) => {
    const data = await controller.create({ publicKey }, seed)
    assert.ok(data, 'success')
    assert.ok(inviter.create.calledOnce, 'calls create on inviter')
    assert.same(omit(data, ['timestamp']), invite, 'invite matches')
    next()
  })

  context('accept: creates a space', async (assert, next) => {
    const data = await controller.accept({ code })
    assert.ok(data, 'success')
    assert.ok(inviter.open.calledOnce, 'opens the code')
    assert.ok(spacesStub.create.calledOnce, 'creates a new space')
    assert.ok(spacesStub.connections.create.calledOnce, 'creates a new connection')
    assert.ok(stubs.log.publish, 'publishes a message')
    next()
  })
})
