const { describe } = require('tape-plus')
const sinon = require('sinon')
const crypto = require('cobox-crypto')
const Repository = require('@coboxcoop/repository')
const randomWords = require('random-words')
const proxyquire = require('proxyquire')
const memdb = require('level-mem')
const { assign, values } = Object

const seedResources = require('../../../fixtures/resource')
const stubResource = require('../../../stubs/resource')
const stubProfile = require('../../../stubs/profile')
const ConnectionsController = require('../../../../app/controllers/spaces/connections')

const { hex } = require('../../../../util')

describe('Controllers: spaces/connections', (context) => {
  let api, stubs, seeds, controller

  context.beforeEach((c) => {
    connections = memdb({ valueEncoding: 'json' })
    stubs = {
      ready: sinon.stub().callsArg(0),
      encryptionKey: hex(crypto.randomBytes(32)),
      swarm: sinon.stub().resolves(true),
      unswarm: sinon.stub().resolves(true),
      db: { connections }
    }

    api = {
      spaces: { store: Repository(null, stubResource(null, stubs)) },
      profile: stubProfile()
    }

    seeds = seedResources()
    api.spaces.store._seed(seeds)
    seed = values(seeds)[values(seeds).length - 1]
    space = api.spaces.store.collection[seed.address]

    spacesStub = { show: sinon.stub().resolves(space) }
    controller = ConnectionsController(api, spacesStub)
  })

  context('index: get space connections', async (assert, next) => {
    var connections = [{
      type: 'put',
      key: hex(crypto.randomBytes(16)),
      value: {
        type: 'peer/connection',
        timestamp: Date.now(),
        content: { connected: true }
      }
    }]

    stubs.db.connections.batch(connections)

    var data = await controller.index(null, seed)
    assert.ok(data, connections, 'gets connections')
    next()
  })

  context('create: start swarming', async (assert, next) => {
    await controller.create(null, seed)
    assert.ok(stubs.swarm.calledOnce, 'calls swarm')
    next()
  })

  context('destroy: stop swarming', async (assert, next) => {
    await controller.destroy(seed)
    assert.ok(stubs.unswarm.calledOnce, 'calls unswarm')
    next()
  })
})
