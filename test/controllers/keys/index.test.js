const { describe } = require('tape-plus')
const httpMocks = require('node-mocks-http')
const path = require('path')
const proxyquire = require('proxyquire')
const sinon = require('sinon')
const Repository = require('@coboxcoop/repository')
const { saveKey } = require('cobox-keys')

const seedResources = require('../../fixtures/resource')
const stubResource = require('../../stubs/resource')

describe('Controllers: keys', (context) => {
  let api, stubs, seeds, controller, dir

  context.beforeEach((c) => {
    stubs = { ready: sinon.stub().callsArg(0) }
    api = {
      settings: { storage: './cobox' },
      spaces: { store: Repository(null, stubResource(null, stubs)) },
    }

    seeds = seedResources()
    api.spaces.store._seed(seeds)

    dir = 'path/to/keys/file'
    createPdf = sinon.stub().callsArg(2)
    ProxyKeysController = proxyquire('../../../app/controllers/keys', {
      'paper-key-backup': { createPdf }
    })

    controller = ProxyKeysController(api)
  })

  context('export: saves a pdf file', async (assert, next) => {
    const keys = await controller.export({ dir })
    assert.ok(keys, 'returns a response')
    assert.ok(createPdf.calledOnce, 'create pdf called')
    assert.same(keys.filename, path.join(dir, 'key-backup.pdf'), 'returns key file path')
    next()
  })
})

