const { describe } = require('tape-plus')
const randomWords = require('random-words')
const ProfileController = require('../../../app/controllers/profile')
const { hex } = require('../../../util')
const sinon = require('sinon')
const crypto = require('cobox-crypto')

describe('Controllers: profile', (context) => {
  let api, stubs, seeds, controller

  context.beforeEach((c) => {
    identity = { name: randomWords(1).pop(), publicKey: crypto.randomBytes(32) }
    profile = Object.assign(identity, { update: sinon.stub().returns(identity) })
    api = { profile }
    controller = ProfileController(api)
  })

  context('show: gets your profile', async (assert, next) => {
    const profile = await controller.show()
    assert.ok(profile, 'returns your profile')
    assert.ok(profile.name, 'returns your name')
    assert.ok(profile.publicKey, 'returns your publicKey')
    next()
  })

  context('update: changes your profile', async (assert, next) => {
    const params = { name: randomWords(1).pop() }
    const profile = await controller.update(params)
    assert.ok(api.profile.update.calledWith(params), 'calls update()')
    next()
  })
})
