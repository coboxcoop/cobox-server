const { describe } = require('tape-plus')
const path = require('path')
const sinon = require('sinon')
const crypto = require('cobox-crypto')
const Repository = require('@coboxcoop/repository')
const Space = require('../../app/models/space')
const Replicator = require('../../app/models/replicator')
const AdminDevice = require('../../app/models/admin/device')
const randomWords = require('random-words')
const Profile = require('../stubs/profile')

const AdminController = require('../../app/controllers/admin')
const EventsController = require('../../app/controllers/events')
const SpacesController = require('../../app/controllers/spaces')
const ReplicatorsController = require('../../app/controllers/replicators')
const ProfileController = require('../../app/controllers/profile')

const AdminValidator = require('../../app/validators/admin')
const SpacesValidator = require('../../app/validators/spaces')
const InviteValidator = require('../../app/validators/invites')
const ReplicatorsValidator = require('../../app/validators/replicators')
const ProfileValidator = require('../../app/validators/profile')
const DeviceStore = require('../mocks/device-store')

const { hex } = require('../../util')
const EventStore = require('../../lib/events')
const { tmp, cleanup } = require('../util')

describe('EventStore', (context) => {
  let api, storage

  context.beforeEach((c) => {
    storage = tmp()

    api = {
      spaces: { store: Repository(path.join(storage, 'spaces'), Space(path.join(storage, 'spaces'))) },
      admin: { devices: { store: Repository(path.join(storage, 'devices'), AdminDevice(path.join(storage, 'devices'))) } },
      replicators: { store: Repository(path.join(storage, 'replicators'), Replicator(path.join(storage, 'replicators'))) },
      devices: DeviceStore(),
      profile: Profile()
    }

    app = {
      controllers: {
        admin: AdminController(api),
        events: EventsController(api),
        spaces: SpacesController(api),
        profile: ProfileController(api),
        replicators: ReplicatorsController(api)
      },
      validators: {
        admin: AdminValidator(api),
        spaces: SpacesValidator(api),
        invites: InviteValidator(api),
        profile: ProfileValidator(api),
        replicators: ReplicatorsValidator(api)
      }
    }
  })

  context('live stream', async (assert, next) => {
    let count = 0

    let data = [
      { resourceType: 'SPACE', data: { type: 'peer/about' } },
      { resourceType: 'SPACE', data: { type: 'space/about' } },
      { resourceType: 'SPACE', data: { type: 'peer/invite' } },
      { resourceType: 'ADMIN_DEVICE', data: { type: 'peer/about' } },
      { resourceType: 'ADMIN_DEVICE', data: { type: 'space/about' } },
      { resourceType: 'ADMIN_DEVICE', data: { type: 'peer/invite' } }
    ]

    const sources = [api.spaces.store, api.replicators.store, api.admin.devices.store]
    const events = EventStore(sources)
    const stream = events.createReadStream()

    stream.on('data', check)

    // create a new space
    var space = await app.controllers.spaces.create({ name: randomWords(1).pop() }, { decorate: true })

    setTimeout(async () => {
      // create a new space invite
      var publicKey = hex(crypto.boxKeyPair().publicKey)
      await app.controllers.spaces.invites.create({ publicKey }, { address: hex(space.address) })

      setTimeout(async () => {
        // create a new admin device
        var device = await app.controllers.admin.devices.create({
          name: randomWords(1).pop(),
          address: hex(crypto.address()),
          encryptionKey: hex(crypto.randomBytes(32))
        }, { decorate: true })

        setTimeout(async () => {
          // create a new admin device invite
          await app.controllers.admin.devices.invites.create({ publicKey }, { address: hex(device.address) })
        }, 10)
      }, 10)
    }, 10)

    function check (msg) {
      if (msg.sync) return
      let check = data[count]
      assert.same(check.resourceType, msg.resourceType, 'correct resourceType')
      assert.same(check.data.type, msg.data.type, 'correct msg type')
      ++count
      done()
    }

    function done () {
      if (count === data.length) {
        stream.destroy()
        return cleanup(storage, next)
      }
    }
  })
})
