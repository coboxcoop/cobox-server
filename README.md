<div align="center">
  <img src="https://cobox.cloud/src/svg/logo.svg">
</div>

# Server

## Table of Contents

* [About](#about)
* [Installing and Updating](#installing-and-updating)
  * [Install & Update Script](#install-and-update-script)
  * [Troubleshooting on Linux](#troubleshooting-on-linux)
  * [Verify Installation](#verify-installation)
  * [NPM Install](#npm-install)
  * [Git Install](#git-install)
* [Usage](#usage)
* [Concepts](#concepts)
* [Docs](#docs)
* [Development](#development)
* [Contributing](#contributing)
* [License](#license)

## About

**CoBox** is an encrypted p2p file system and distributed back-up tool. You can read more at [develop.cobox.cloud/](https://develop.cobox.cloud/).

CoBox was developed using funding from the [EU Ledger NGI](https://ledgerproject.eu/) programme.

Its aim is to facilitate a transition to a sovereign commons-based data infrastructure and a co-operative distributed cloud architecture.

CoBox is built on Dat. Dat is a modular peer-to-peer technology stack. You can find a good explanation of how it works in the guide ['how Dat works’](https://datprotocol.github.io/how-dat-works/), and you can find out how we’ve made use of Dat in [our developer docs](https://develop.cobox.cloud/core_technologies.html). CoBox uses the hypercore protocol and the hyperswarm distributed hash table (DHT) to connect peers in a distributed network. All data is encrypted locally, before being replicated securely across a swarm of peers.

For CoBox, we’ve used Dat to build fully encrypted private spaces that synchronise seamlessly across multiple devices. This means you, your friends and colleagues no longer have to rely on corporate servers to store your files, you can hold onto them for each-other.

CoBox Server is a core component of the CoBox App. It exposes a VueJS UI, an ExpressJS JSON API, and a Yargs CLI for interacting with CoBox 'spaces', 'backups' and 'seeders'. Check out our [concepts](#concepts) section for more details on what a space is, how backups work and... what's a 'seeder'?

## Installing and Updating

### Install and Update Script

To install or update cobox-server, you should run the [install script](). To do that, you may either download and run the script manually, or use the following cURL or Wget command:

```
curl -o- https://cobox.cloud/releases/cobox-app_v1.0.0-beta1/download.sh | bash
```

```
wget -qO- https://cobox.cloud/releases/cobox-app_1.0.0-beta1/download.sh | bash
```

Running either of the above commands downloads a script and runs it. The script downloads a tarball that contains the release binary required by your operating system with additional assets and unpacks the contents to the install directory.

### Troubleshooting on Linux

On Linux, after running the install script, if you get `cobox: command not found` or see no feedback from your terminal after you type `command -v cobox`, simply close your current terminal, open a new terminal, and try verifying again.

### Verify Installation

To verify that CoBox has been installed correctly, do:

```
command -v cobox
```

We strongly recommend you produce a checksum of your download to ensure that our code hasn't been tampered with.

```
sha256sum ~/.cobox/releases/latest/cobox
```

This will vary depending on the version you have downloaded.

```
cobox --version
```

You can view a list of published versions and their checksums [on our website]().

### NPM Install

```
npm install -g @coboxcoop/server

cobox
```

### Git Install

Before you get started, ensure you have `git` and `node` installed. We currently release using `v12.16.3` which we recommend using. You can download and install nodejs from your distribution package manager, or [direct from their website](https://nodejs.org/en/download/).

```
# clone the repository
git clone http://gitlab.com/coboxcoop/cobox-server && cd cobox-server

npm install -g yarn

# install dependencies
yarn

# start the app
yarn start

# use the cli
node bin
```

## Usage

CoBox Server is accessible through a UI in the browser, and through a CLI in the terminal.

```
CoBox 1.0 - an encrypted peer-to-peer file system and distributed back-up tool

  Copyright (C) 2019 Magma Collective T&DT, License GNU AGPL v3+
  This is free software: you are free to change and redistribute it
  For the latest sourcecode go to <https://code.cobox.cloud/>

Usage: cobox <command> [options]

Commands:
  cobox up [options]       start the app
  cobox whoami             display your nickname and public key
  cobox nickname <name>    change your nickname
  cobox spaces <command>   view and interact with your spaces
  cobox seeders <command>  register, pair with and send commands to your seeder(s)
  cobox backups <command>  back-up your peers' spaces locally
  cobox export [options]   export your keys
  cobox import [options]   import your keys
  cobox setup <command>    setup options and settings
  cobox down [options]     stop the app

Options:
  -h, --help, --help        Show help                                  [boolean]
  -v, --version, --version  Show version number                        [boolean]

For more information on cobox read the manual: man cobox
Please report bugs on <http://gitlab.com/coboxcoop/core/issues>.
```

## Concepts

To be written

## Docs

To be written

# Development

If you want to develop, we recommend using [Node Version Manager](https://github.com/nvm-sh/nvm), [Yarn](https://yarnpkg.com/) and we also suggest you check out [our monorepo](https://gitlab.com/coboxcoop/core) which makes use of yarn workspaces and git modules to help ease the development of multiple cross-dependent packages. The mono-repo contains [instructions](https://gitlab.com/coboxcoop/core/-/blob/master/DEVELOPMENT.md) on how to get setup to develop and extend the CoBox APIs, as well as some resources on how to use the tools we make use of and make merge requests.

# Contributing

To be written

# License

[`AGPL-3.0-or-later`](./LICENSE)
