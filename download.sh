#!/bin/bash

# Downloads and installs cobox-server
# usage: wget -qO- https://gitlab.com/coboxcoop/cobox-server/-/raw/master/download.sh | bash

BIN=$HOME/bin/cobox-server
APP_DIR=$HOME/.cobox/releases

# hard coded latest version as can't figure out how to get latest from gitlab
LATEST="1.0.0-alpha1"
# gitlab project id hard coded
GITLAB_PROJECT_ID=16458528

fail () {
  msg=$1
  echo "============"
  echo "Error: $msg" 1>&2
  exit 1
}

function cleanup {
  rm -rf $APP_DIR/tmp.tar.gz > /dev/null
}

install () {
  [ ! "$BASH_VERSION" ] && fail "Please use bash instead"
  GET=""
  if which curl > /dev/null; then
    GET="curl"
    GET="$GET --fail -# -L"
  elif which wget > /dev/null; then
    GET="wget"
    GET="$GET -qO-"
  else
    fail "neither wget/curl are installed"
  fi
  case `uname -s` in
    Darwin) OS="darwin";;
    Linux) OS="linux";;
    *) fail "unsupported os: $(uname -s)";;
  esac
  if uname -m | grep 64 > /dev/null; then
    ARCH="x64"
  else
    fail "only arch x64 is currently supported for single file install. please use npm instead. your arch is: $(uname -m)"
  fi
  mkdir -p $APP_DIR || fail "Could not create directory $APP_DIR, try manually downloading and extracting instead."
  cd $APP_DIR
  RELEASE="cobox-server-${LATEST}-${OS}-${ARCH}"
  URL="https://gitlab.com/coboxcoop/cobox-server/-/archive/v$LATEST/$RELEASE.tar.gz"
  echo "Downloading $URL"
  bash -c "$GET $URL" > $APP_DIR/tmp.tar.gz || fail "download failed"
  tar xz ./$RELEASE
  BIN="$APP_DIR/$RELEASE/cobox-server"
  chmod +x $BIN || fail "chmod +x failed"
  cleanup
  echo "CoBox server $LATEST has been downloaded successfully. Execute it with this command:\n\n${BIN}\n"
  echo "Add it to your PATH with this command (add this to .bash_profile / .bashrc / .zshrc):\n\nexport PATH=\"\$PATH:$APP_DIR/$RELEASE\"\n"
}

install
