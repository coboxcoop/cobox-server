exports.command = 'setup <command>'
exports.describe = 'setup options and settings'
exports.handler = function () {}
exports.builder = function (yargs) {
  return yargs
    .commandDir('setup')
    .demandCommand()
    .help()
}
