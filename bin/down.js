const Client = require('cobox-client')
const { buildBaseURL } = require('cobox-client/util')
const options = require('./lib/options')

exports.command = 'down [options]'
exports.desc = 'stop the client app'
exports.builder = { port: options.port }
exports.handler = stop

async function stop (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  return client.get('stop')
}
