const fs = require('fs')
const { exec } = require('child_process')
const debug = require('debug')
const yargs = require('yargs')
const path = require('path')

const options = require('../lib/options')

const {
  DEFAULT_COLOUR,
  onSuccess,
  onError
} = require('../lib/util')

exports.command = 'fuse [options]'
exports.desc = 'configure fuse'
exports.builder = { mount: options.mount }
exports.handler = configureFUSE

function configureFUSE (argv) {
  const user = process.geteuid()
  const space = process.getgid()
  const mountdir = path.resolve(argv.mount)

  onSuccess(`Setting up FUSE`)

  checkLocationExists(mountdir, (err, stat) => {
    if (err) return onError(err.message)
    if (stat) {
      onSuccess([`Desired mount point ${mountdir} already exists..`, `Assessing permissions`])

      checkPermissions(stat, (err, can) => {
        if (can) {
          onSuccess(`Correct permissions set`)
          return onfinish()
        }
        else return setPermissions(mountdir, onfinish)
      })
    } else {
      onError(`${mountdir} does not yet exist, creating...`)

      checkLocationExists(path.dirname(mountdir), (err, stat) => {
        checkPermissions(stat, (err, can) => {
          if (can) return exec(`mkdir -p ${mountdir}`, (err) => {
            if (err) return onfinish(new Error(`Could not create the ${mountdir} directory.`)) // TODO: pass error code 
            else return onfinish()
          })

          return exec(`sudo mkdir -p ${mountdir}`, (err) => {
            if (err) return onfinish(new Error(`Could not create the ${mountdir} directory.`)) // TODO: pass error code 
            return setPermissions(mountdir, onfinish)
          })
        })
      })
    }
  })

  function onfinish (err) {
    if (err) return onError(err.message)
    return onSuccess([`Mount point set to ${mountdir}`, `Successfully configured FUSE`])
  }

  function checkLocationExists (location, cb) {
    fs.stat(location, (err, stat) => {
      if (err && err.errno !== -2) return cb(new Error(`Could not get the status of ${location}.`))
      else cb(null, stat)
    })
  }

  function checkPermissions (stat, cb) {
    return cb(null, stat.uid === user && stat.gid === space && !!(2 & parseInt((stat.mode & parseInt("777", 8)).toString (8)[0])))
  }

  function setPermissions (location, cb) {
    exec(`sudo chown ${user}:${space} ${location}`, (err) => {
      if (err) return cb(new Error(`Could not change the permissions on the ${location} directory.`))
      return cb()
    })
  }
}
