const pm2 = require('pm2')
const debug = require('debug')('cobox-server:cli')
const path = require('path')
const { spawn } = require('child_process')
const options = require('./lib/options')
const { printAppInfo } = require('../util')

if (require.main === module) return up(require('yargs').options(options).argv)

const DEV_DEBUG = '*,-express*,-hypercore-protocol*,-bodyparser*,-babel*'

exports.command = 'up [options]'
exports.desc = 'start the client app'
exports.builder = options
exports.handler = up

function up (argv) {
  const args = copyArgs(argv, ['port', 'hostname', 'storage', 'dev', 'mount', 'udpPort', 'withUi'])
  const script = require.resolve('../main.js')

  if (argv.dev) return startDevelopmentServer(args)
  else return startProductionServer(args)

  function startProductionServer (args) {
    pm2.connect((err) => {
      if (err) process.exit(2)

      var opts = {
        name: require(require.resolve('../package.json')).name,
        script,
        args,
        max_memory_restart: '100M',
        autorestart: false
      }

      pm2.start(opts, (err, apps) => {
        if (err) throw err
        printAppInfo(argv)
        pm2.disconnect()
      })
    })
  }

  function startDevelopmentServer (args) {
    debug('starting server in developer\'s mode')
    process.env.DEBUG = process.env.DEBUG || DEV_DEBUG
    process.env.NODE_ENV = process.env.NODE_ENV || 'development'

    args.unshift(script)

    spawn(process.execPath, args, {
      env: {
        ...process.env,
        FORCE_COLOR: process.env.FORCE_COLOR || '2'
      },
      stdio: 'inherit'
    })
  }
}

function copyArgs (from, keys) {
  const args = []

  for (const key of keys) {
    if (from[key] !== undefined) {
      args.push(argkey(key))
      if (from[key] !== true) {
        args.push(from[key])
      }
    }
  }
  return args

  function argkey (key) {
    return '--' + key.replace(
      /[\w]([A-Z])/g,
      m => m[0] + '-' + m[1]
    ).toLowerCase()
  }
}

function noop () {}

