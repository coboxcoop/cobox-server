const constants = require('cobox-constants')

module.exports = {
  port: {
    alias: 'p',
    number: true,
    describe: 'cobox server port',
    default: 9112
  },
  udpPort: {
    alias: 'b',
    number: true,
    describe: 'hub udp packet port',
    default: 8999
  },
  hostname: {
    describe: 'hostname',
    default: 'localhost'
  },
  storage: {
    alias: 's',
    type: 'string',
    describe: 'location of cobox application',
    default: constants.storage
  },
  mount: {
    alias: 'm',
    type: 'string',
    describe: "mount path for your space's file systems",
    default: constants.mount
  },
  dev: {
    describe: 'start in developer mode'
  }
}
