#!/usr/bin/env node

const debug = require('debug')('cobox-server:cli')
const yargs = require('yargs')

const usage = `CoBox 1.0 - an encrypted peer-to-peer file system and distributed back-up tool

  Copyright (C) 2019 Magma Collective T&DT, License GNU AGPL v3+
  This is free software: you are free to change and redistribute it
  For the latest sourcecode go to <https://code.cobox.cloud/>

Usage: $0 <command> [options]`

const epilogue = `For more information on cobox read the manual: man cobox
Please report bugs on <http://gitlab.com/coboxcoop/core/issues>.`

if (require.main === module) return require('cobox-cli')
  .usage(usage)
  .commandDir('bin')
  .demandCommand()
  .alias('h', 'help')
  .alias('v', 'version')
  .help()
  .epilogue(epilogue)
  .argv

exports.command = '$0 <command>'
exports.describe = 'peer-to-peer filesystems and distributed encrypted backups'
exports.handler = () => {}
exports.builder = (yargs) => {
  return yargs
    .usage('Usage: $0 <command> [options]')
    .commandDir('bin')
    .demandCommand()
    .help()
}

module.exports = exports
